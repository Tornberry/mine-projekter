import GameSetup.Game;
import Models.*;

import java.io.IOException;
import java.lang.System;
import java.util.ArrayList;
import java.util.List;

public class LaunchGame {

    public static void main(String[] args) throws IOException {

        //En setup klasse, som med metoden SetupGame udfører nødvendige ting ved spillets start. Udover det holder den en colorpicker og en racepicker.
        Game game = new Game();

        List<Player> playerList = new ArrayList<>();
        Player redPlayer = new Player("Simon", game.gameMap.baseMap().racePicker.pickRandomRace(), game.gameMap.baseMap().colorPicker.pickRed());
        Player bluePlayer = new Player("Lasse", game.gameMap.baseMap().racePicker.pickRandomRace(), game.gameMap.baseMap().colorPicker.pickBlue());
        playerList.add(redPlayer);
        playerList.add(bluePlayer);

        game.addUnitToSystem(game.gameMap.baseMap().unitPicker.Dreadnought(), game.Galaxy().Center(),bluePlayer);
        game.addUnitToSystem(game.gameMap.baseMap().unitPicker.Dreadnought(), game.Galaxy().Center(),bluePlayer);
        game.addUnitToSystem(game.gameMap.baseMap().unitPicker.Destroyer(),game.Galaxy().Center(), bluePlayer);
        game.addUnitToSystem(game.gameMap.baseMap().unitPicker.Dreadnought(), game.Galaxy().Center(),redPlayer);
        game.addUnitToSystem(game.gameMap.baseMap().unitPicker.Dreadnought(), game.Galaxy().Center(),redPlayer);
        game.addUnitToSystem(game.gameMap.baseMap().unitPicker.Dreadnought(), game.Galaxy().Center(),redPlayer);
        game.addUnitToSystem(game.gameMap.baseMap().unitPicker.Dreadnought(), game.Galaxy().Center(),redPlayer);

        game.addUnitToSystem(game.gameMap.baseMap().unitPicker.Cruiser(), game.Galaxy().North(), redPlayer);
        game.addUnitToSystem(game.gameMap.baseMap().unitPicker.Cruiser(), game.Galaxy().North(), redPlayer);
        game.addUnitToSystem(game.gameMap.baseMap().unitPicker.Carrier(), game.Galaxy().North(), redPlayer);

        game.Galaxy().findAllInfo();

        Player winner = game.resolveSpaceBattle(game.Galaxy().Center(), bluePlayer, redPlayer);


    }
}
