package Models.Units;

import Models.Player;

public class Cruiser implements Units {

    //En cruiser har en række predifinerede værdier, som ved instantiering af klasse forud-initialiseres.
    //En carrier har også en ejer.
    int resourceCost = 2;
    int combatValue = 7;
    int movementSpeed = 2;
    int capacity = 0;
    Player owner;

    //Når man instantiere en cruiser, behøver mand at angive ejeren af cruiseren.
    public Cruiser(Player owner) {
        this.owner = owner;
    }

    //Metoder beskrivet i 'Units' interfacet. Læs i interface klasse for beskrivelse.----------
    @Override
    public int getResourceCost() {
        return resourceCost;
    }

    @Override
    public int getCombatValue() {
        return combatValue;
    }

    @Override
    public int getMovementSpeed() {
        return movementSpeed;
    }

    @Override
    public int getCapacity() {
        return capacity;
    }

    @Override
    public Player getOwner() {
        return owner;
    }
    //------------------------------------------------------------------------------------------
}
