package Models.Units;

import Models.Player;

public interface Units {
    // I dette interface er der beskrivet nødvendige getters for mine spaceship klasser
    int getResourceCost();
    int getCombatValue();
    int getMovementSpeed();
    int getCapacity();
    Player getOwner();
}
