package Models.Units;

import Models.Player;

public class Dreadnought implements Units {

    //En dreadnought har en række predifinerede værdier, som ved instantiering af klasse forud-initialiseres.
    //En dreadnought har også en ejer.
    int resourceCost = 5;
    int combatValue = 5;
    int movementSpeed = 1;
    int capacity = 0;
    Player owner;

    //Når man instantiere en dreadnought, behøver mand at angive ejeren af dreadnoughten.
    public Dreadnought(Player owner) {
        this.owner = owner;
    }

    //Metoder beskrivet i 'Units' interfacet. Læs i interface klasse for beskrivelse.----------
    @Override
    public int getResourceCost() {
        return resourceCost;
    }

    @Override
    public int getCombatValue() {
        return combatValue;
    }

    @Override
    public int getMovementSpeed() {
        return movementSpeed;
    }

    @Override
    public int getCapacity() {
        return capacity;
    }

    @Override
    public Player getOwner() {
        return owner;
    }
    //------------------------------------------------------------------------------------------
}
