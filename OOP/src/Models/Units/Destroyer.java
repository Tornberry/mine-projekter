package Models.Units;

import Models.Player;

public class Destroyer implements Units {

    //En destroyer har en række predifinerede værdier, som ved instantiering af klasse forud-initialiseres.
    //En destroyer har også en ejer.
    int resourceCost = 1;
    int combatValue = 9;
    int movementSpeed = 2;
    int capacity = 0;
    Player owner;

    //Når man instantiere en destroyer, behøver mand at angive ejeren af destroyeren.
    public Destroyer(Player owner) {
        this.owner = owner;
    }

    //Metoder beskrivet i 'Units' interfacet. Læs i interface klasse for beskrivelse.----------
    @Override
    public int getResourceCost() {
        return resourceCost;
    }

    @Override
    public int getCombatValue() {
        return combatValue;
    }

    @Override
    public int getMovementSpeed() {
        return movementSpeed;
    }

    @Override
    public int getCapacity() {
        return capacity;
    }

    @Override
    public Player getOwner() {
        return owner;
    }
    //------------------------------------------------------------------------------------------
}
