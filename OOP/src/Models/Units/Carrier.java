package Models.Units;

import Models.Player;

public class Carrier implements Units {

    //En carrier har en række predefinerede værdier, som ved instantiering af klasse forud-initialiseres.
    //En carrier har også en ejer.
    int resourceCost = 3;
    int combatValue = 9;
    int movementSpeed = 1;
    int capacity = 6;
    Player owner;

    //Når man instantierer en carrier, er det nødvendigt at angive ejeren af carrieren.
    public Carrier(Player owner) {
        this.owner = owner;
    }


    //Metoder beskrivet i 'Units' interfacet. Læs i interface klasse for beskrivelse.----------
    @Override
    public int getResourceCost() {
        return resourceCost;
    }

    @Override
    public int getCombatValue() {
        return combatValue;
    }

    @Override
    public int getMovementSpeed() {
        return movementSpeed;
    }

    @Override
    public int getCapacity() {
        return capacity;
    }

    @Override
    public Player getOwner() {
        return owner;
    }
    //------------------------------------------------------------------------------------------
}
