package Models;

import Models.Units.Carrier;
import Models.Units.Cruiser;
import Models.Units.Destroyer;
import Models.Units.Dreadnought;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class System {
    String name;
    List<System> adjacentSystems = new ArrayList<>();
    List<Planet> planets = new ArrayList<>();
    List<Carrier> carriers = new ArrayList<>();
    List<Cruiser> cruisers = new ArrayList<>();
    List<Destroyer> destroyers = new ArrayList<>();
    List<Dreadnought> dreadnoughts = new ArrayList<>();

    public System(String name) {
        this.name = name;
    }

    public List<Planet> getPlanets() {
        return planets;
    }

    public void setPlanets(List<Planet> planets) {
        this.planets = planets;
    }

    public void addPlanet(Planet planet){
        this.planets.add(planet);
    }

    public String getName() {
        return name;
    }

    public void addAdjacentSystem(System system){
        this.adjacentSystems.add(system);
    }

    public ArrayList<System> getAdjacentSystems() { ;
        return new ArrayList<System>(adjacentSystems);
    }

    public List<Carrier> getCarriers() {
        return carriers;
    }

    public void addCarrier(Player owner) {
        this.carriers.add(new Carrier(owner));
    }

    public void removeCarrierByPlayer(Player player){
        boolean removedOne = false;

        for (Carrier carrier : this.carriers){
            if(carrier.getOwner() == player) {
                carriers.remove(carrier);
                removedOne = true;
                break;
            } else {

            }
        }
        if (removedOne){
            java.lang.System.out.println("You succesfully removed a carrier for player " + "'" + player.getPlayerName() + "'");
        } else {
            java.lang.System.out.println("This player does not have a carrier on this system");
        }
    }

    public List<Cruiser> getCruisers() {
        return cruisers;
    }

    public void addCruiser(Player owner) {
        this.cruisers.add(new Cruiser(owner));
    }

    public void removeCruiserByPlayer(Player player){
        boolean removedOne = false;

        for (Cruiser cruiser : this.cruisers){
            if(cruiser.getOwner() == player) {
                carriers.remove(cruiser);
                removedOne = true;
                break;
            } else {

            }
        }
        if (removedOne){
            java.lang.System.out.println("You succesfully removed a cruiser for player " + "'" + player.getPlayerName() + "'");
        } else {
            java.lang.System.out.println("This player does not have a cruiser on this system");
        }
    }

    public List<Destroyer> getDestroyers() {
        return destroyers;
    }

    public void addDestroyer(Player owner) {
        this.destroyers.add(new Destroyer(owner));
    }

    public void removeDestroyerByPlayer(Player player){
        boolean removedOne = false;

        for (Destroyer destroyer : this.destroyers){
            if(destroyer.getOwner() == player) {
                carriers.remove(destroyer);
                removedOne = true;
                break;
            } else {

            }
        }
        if (removedOne){
            java.lang.System.out.println("You succesfully removed a destroyer for player " + "'" + player.getPlayerName() + "'");
        } else {
            java.lang.System.out.println("This player does not have a destroyer on this system");
        }
    }

    public List<Dreadnought> getDreadnoughts() {
        return dreadnoughts;
    }

    public void addDreadnought(Player owner) {
        this.dreadnoughts.add(new Dreadnought(owner));
    }

    public void removeDreadnoughtByPlayer(Player player){
        boolean removedOne = false;

        for (Dreadnought dreadnought : this.dreadnoughts){
            if(dreadnought.getOwner() == player) {
                carriers.remove(dreadnought);
                removedOne = true;
                break;
            } else {

            }
        }
        if (removedOne){
            java.lang.System.out.println("You succesfully removed a dreadnought for player " + "'" + player.getPlayerName() + "'");
        } else {
            java.lang.System.out.println("This player does not have a dreadnought on this system");
        }
    }

    public void showAllSpaceShipsInSystem(System system){
        java.lang.System.out.println("List of spaceships in system " + system.getName());
        java.lang.System.out.println("- Amount of carriers: " + system.getCarriers().size());
        java.lang.System.out.println("- Amount of cruisers: " + system.getCruisers().size());
        java.lang.System.out.println("- Amount of destroyers: " + system.getDestroyers().size());
        java.lang.System.out.println("- Amount of dreadnoughts: " + system.getDreadnoughts().size());
    }
}
