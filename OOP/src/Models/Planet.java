package Models;

import java.util.Objects;

public class Planet {
    String name;
    int resourceProduction;
    Boolean isInGalaxy = false;

    public Planet(String name, int resourceProduction) {
        this.name = name;
        this.resourceProduction = resourceProduction;
    }

    public Boolean isInGalaxy() {
        return isInGalaxy;
    }

    public void setInGalaxy(Boolean inGalaxy) {
        isInGalaxy = inGalaxy;
    }

    public String getName() {
        return name;
    }

    public int getResourceProduction() {
        return resourceProduction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Planet planet = (Planet) o;
        return resourceProduction == planet.resourceProduction &&
                Objects.equals(name, planet.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, resourceProduction);
    }


}
