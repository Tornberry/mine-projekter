package Models;

import Models.Planet;
import Models.System;
import Models.Units.Carrier;
import Models.Units.Cruiser;
import Models.Units.Destroyer;
import Models.Units.Dreadnought;

import java.util.ArrayList;
import java.util.List;

public class Galaxy {

    //En galakse er en samling af systemer. Da en galakse er det 'map' man spiller på, kan der ikke være flere
    //galakser i en session. Derfor er der ikke nogle properties der beskriver en galakse.
    List<System> systems = new ArrayList<>();

    //En metode som returnere en liste af alle de systemer som findes i galaksen
    public List<System> getSystems() {
        return systems;
    }

    //En metode, som tilføjer et system til galaksen. Systemet man ønsker at tilføje, giver man når metoden kaldes
    public void addSystem(System system){
        this.systems.add(system);
    }

    //En metode, der for hvert system i galaksen, printer en besked om at der er fundet et system samt id'et på systemet.
    //Altså en liste af alle systemer i galaksen
    public void findAllSystems() {
        java.lang.System.out.println("List of systems found in this galaxy");
        for (System system : systems) {
            java.lang.System.out.println("- System with id " + system.getName() + " found in galaxy");
        }
    }

    //En metode, der for hvert system i galaksen, printer ud hvor mange af hvert rumskibstype der er i systemet
    public void findShipsOnAllSystems(){
        for (System system : systems) {
            java.lang.System.out.println("List of spaceships for system with id: " + system.getName());
            java.lang.System.out.println("- Amount of carriers: " + system.getCarriers().size());
            java.lang.System.out.println("- Amount of cruisers: " + system.getCruisers().size());
            java.lang.System.out.println("- Amount of destroyers: " + system.getDestroyers().size());
            java.lang.System.out.println("- Amount of dreadnoughts: " + system.getDreadnoughts().size());
        }
    }

    //En metode, som for hvert system i galaksen, printer en besked om en fundet planet og dens navn.
    //Altså en liste af planeter i hvert system i galaksen
    public void findPlanetsOnAllSystems(){
        for (System system : systems){
            java.lang.System.out.println("List of planets for system with id: " + system.getName());
            for (Planet planet : system.getPlanets()) {
                java.lang.System.out.println("- Found planet with name: " + planet.getName());
            }
        }
    }

    //En metode der printer alle de ovenstående informationer på en struktureret måde.
    public void findAllInfo(){
        findAllSystems();
        java.lang.System.out.println("----------------------------------------");
        findShipsOnAllSystems();
        java.lang.System.out.println("----------------------------------------");
        findPlanetsOnAllSystems();
    }

    private List<Carrier> getAllCarriersByPlayer(Player player){
        List<Carrier> carriers = new ArrayList<>();

        for (System system : this.getSystems()){
            for (Carrier carrier : system.getCarriers()) {
                if (carrier.getOwner() == player) {
                    carriers.add(carrier);
                }
            }
        }

        return carriers;
    }

    private List<Cruiser> getAllCruisersByPlayer(Player player){
        List<Cruiser> cruisers = new ArrayList<>();

        for (System system : this.getSystems()){
            for (Cruiser cruiser : system.getCruisers()){
                if (cruiser.getOwner() == player){
                    cruisers.add(cruiser);
                }
            }
        }

        return cruisers;
    }

    private List<Destroyer> getAllDestroyersByPlayer(Player player){
        List<Destroyer>  destroyers = new ArrayList<>();

        for (System system : this.getSystems()){
            for (Destroyer destroyer : system.getDestroyers()){
                if (destroyer.getOwner() == player){
                    destroyers.add(destroyer);
                }
            }
        }

        return  destroyers;
    }

    private List<Dreadnought> getAllDreadnoughtsByPlayer(Player player){
        List<Dreadnought> dreadnoughts = new ArrayList<>();

        for (System system : this.getSystems()){
            for (Dreadnought dreadnought : system.getDreadnoughts()){
                if (dreadnought.getOwner() == player){
                    dreadnoughts.add(dreadnought);
                }
            }
        }

        return dreadnoughts;
    }

    private List<Object> collectAllUnitsByPlayerSorted(Player player){
        List<Object> units = new ArrayList<>();

        for (Carrier carrier : getAllCarriersByPlayer(player)){
            units.add(carrier);
        }

        for (Destroyer destroyer : getAllDestroyersByPlayer(player)){
            units.add(destroyer);
        }

        for (Cruiser cruiser : getAllCruisersByPlayer(player)){
            units.add(cruiser);
        }

        for (Dreadnought dreadnought : getAllDreadnoughtsByPlayer(player)){
            units.add(dreadnought);
        }

        return units;
    }

    public void getAllUnitsByPlayerSorted(Player player) {
        java.lang.System.out.println("List of units found in galaxy for player " + player.getPlayerName() + " sorted by combat value and resource cost");
        for (Object unit : collectAllUnitsByPlayerSorted(player)){
            if (unit.getClass() == Carrier.class){
                java.lang.System.out.println("- Carrier");
            } else if (unit.getClass() == Cruiser.class){
                java.lang.System.out.println("- Cruiser");
            } else if (unit.getClass() == Destroyer.class){
                java.lang.System.out.println("- Destroyer");
            } else if (unit.getClass() == Dreadnought.class){
                java.lang.System.out.println("- Dreadnought");
            }
        }
    }

    public System North(){
        return systems.get(0);
    }

    public System North_East(){
        return systems.get(1);
    }

    public System South_East(){
        return systems.get(2);
    }

    public System South(){
        return systems.get(3);
    }

    public System South_West(){
        return systems.get(4);
    }

    public System North_West(){
        return systems.get(5);
    }

    public System Center(){
        return systems.get(6);
    }
}
