package Models;

import Models.Units.Carrier;
import Models.Units.Cruiser;
import Models.Units.Destroyer;
import Models.Units.Dreadnought;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Player {

    private String playerName;
    private Race race;
    private String color;
    private List<Carrier> carriers = new ArrayList<>();
    private List<Cruiser> cruisers = new ArrayList<>();
    private List<Destroyer> destroyers = new ArrayList<>();
    private List<Dreadnought> dreadnoughts = new ArrayList<>();


    public Player(String playerName, Race race, String color) {
        this.playerName = playerName;
        this.race = race;
        this.color = color;
    }

    public void addCarrier(){
        this.carriers.add(new Carrier(this));
    }

    public void addCruiser(){
        this.cruisers.add(new Cruiser(this));
    }

    public void addDestroyer(){
        this.destroyers.add(new Destroyer(this));
    }

    public void addDreadnought(){
        this.dreadnoughts.add(new Dreadnought(this));
    }

    public String getPlayerName() {
        return playerName;
    }

    public Race getRace() {
        return race;
    }

    public String getColor() {
        return color;
    }

    public List<Carrier> getCarriers() {
        return carriers;
    }

    public List<Cruiser> getCruisers() {
        return cruisers;
    }

    public List<Destroyer> getDestroyers() {
        return destroyers;
    }

    public List<Dreadnought> getDreadnoughts() {
        return dreadnoughts;
    }

    public void setCarriers(List<Carrier> carriers) {
        this.carriers = carriers;
    }

    public void setCruisers(List<Cruiser> cruisers) {
        this.cruisers = cruisers;
    }

    public void setDestroyers(List<Destroyer> destroyers) {
        this.destroyers = destroyers;
    }

    public void setDreadnoughts(List<Dreadnought> dreadnoughts) {
        this.dreadnoughts = dreadnoughts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return Objects.equals(playerName, player.playerName) &&
                Objects.equals(race, player.race) &&
                Objects.equals(color, player.color) &&
                Objects.equals(carriers, player.carriers) &&
                Objects.equals(cruisers, player.cruisers) &&
                Objects.equals(destroyers, player.destroyers) &&
                Objects.equals(dreadnoughts, player.dreadnoughts);
    }

    @Override
    public int hashCode() {

        return Objects.hash(playerName, race, color, carriers, cruisers, destroyers, dreadnoughts);
    }

    @Override
    public String toString() {
        return "Player{" +
                "playerName='" + playerName + '\'' +
                ", race=" + race +
                ", color=" + color +
                ", carriers=" + carriers +
                ", cruisers=" + cruisers +
                ", destroyers=" + destroyers +
                ", dreadnoughts=" + dreadnoughts +
                '}';
    }
}
