package GameSetup.Maps;

import GameSetup.Maps.MapSupport.Map;
import GameSetup.Maps.MapSupport.NewMap;

public class iDontKnow implements NewMap {
    @Override
    public void populateSystems() {
        map.AddPlanetToSystem(map.planetPicker.Mecatol_Rex(), map.System7());
        map.AddPlanetToSystem(map.planetPicker.Mellon(), map.System4());
    }

    @Override
    public Map baseMap() {
        return map;
    }
}
