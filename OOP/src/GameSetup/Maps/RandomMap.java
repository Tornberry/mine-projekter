package GameSetup.Maps;

import GameSetup.Maps.MapSupport.Map;
import GameSetup.Maps.MapSupport.NewMap;

import java.util.Random;

public class RandomMap implements NewMap {

    Map map = new Map();
    Random randomNumber = new Random();

    @Override
    public void populateSystems() {
        for(int i=0; i==randomNumber.nextInt(4); i++){
            map.AddPlanetToSystem(map.planetPicker.RandomPlanet(), map.System1());
        }

        for(int i=0; i==randomNumber.nextInt(4); i++){
            map.AddPlanetToSystem(map.planetPicker.RandomPlanet(), map.System2());
        }

        for(int i=0; i==randomNumber.nextInt(4); i++){
            map.AddPlanetToSystem(map.planetPicker.RandomPlanet(), map.System3());
        }

        for(int i=0; i==randomNumber.nextInt(4); i++){
            map.AddPlanetToSystem(map.planetPicker.RandomPlanet(), map.System4());
        }

        for(int i=0; i==randomNumber.nextInt(4); i++){
            map.AddPlanetToSystem(map.planetPicker.RandomPlanet(), map.System5());
        }

        for(int i=0; i==randomNumber.nextInt(4); i++){
            map.AddPlanetToSystem(map.planetPicker.RandomPlanet(), map.System6());
        }

        map.AddPlanetToSystem(map.planetPicker.Mecatol_Rex(), map.System7());

    }

    @Override
    public Map baseMap() {
        return map;
    }
}
