package GameSetup.Maps;

import GameSetup.Maps.MapSupport.Map;
import GameSetup.Maps.MapSupport.NewMap;

public class Opgave7Map implements NewMap {

    @Override
    public void populateSystems(){

        map.AddPlanetToSystem(map.planetPicker.Vega_Minor(), map.System1());
        map.AddPlanetToSystem(map.planetPicker.Vega_Major(), map.System1());
        map.AddPlanetToSystem(map.planetPicker.Industrex(), map.System3());
        map.AddPlanetToSystem(map.planetPicker.Rigel_I(), map.System4());
        map.AddPlanetToSystem(map.planetPicker.Rigel_II(), map.System4());
        map.AddPlanetToSystem(map.planetPicker.Mirage(), map.System6());
        map.AddPlanetToSystem(map.planetPicker.Mecatol_Rex(), map.System7());

    }

    @Override
    public Map baseMap() {
        return map;
    }


}
