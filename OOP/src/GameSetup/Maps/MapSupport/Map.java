package GameSetup.Maps.MapSupport;

import GameSetup.Support.Pickers.ColorPicker;
import GameSetup.Support.Pickers.PlanetPicker;
import GameSetup.Support.Pickers.RacePicker;
import GameSetup.Support.Pickers.UnitPicker;
import Models.Galaxy;
import Models.Planet;
import Models.System;

import java.util.ArrayList;
import java.util.List;

public class Map {

    private System system1 = new System("North");
    private System system2 = new System("North East");
    private System system3 = new System("South East");
    private System system4 = new System("South");
    private System system5 = new System("South West");
    private System system6 = new System("North West");
    private System system7 = new System("Center");

    public RacePicker racePicker = new RacePicker();
    public ColorPicker colorPicker = new ColorPicker();
    public UnitPicker unitPicker = new UnitPicker();
    public PlanetPicker planetPicker = new PlanetPicker();

    public Map() {
        makeMap();
    }

    public void AddPlanetToSystem(Planet planet, System system){
        system.addPlanet(planet);
    }

    private void generateSystemAdjacency(){
        system1.addAdjacentSystem(system2);
        system1.addAdjacentSystem(system7);
        system1.addAdjacentSystem(system6);

        system2.addAdjacentSystem(system1);
        system2.addAdjacentSystem(system7);
        system2.addAdjacentSystem(system3);

        system3.addAdjacentSystem(system2);
        system3.addAdjacentSystem(system7);
        system3.addAdjacentSystem(system4);

        system4.addAdjacentSystem(system5);
        system4.addAdjacentSystem(system7);
        system4.addAdjacentSystem(system3);

        system5.addAdjacentSystem(system6);
        system5.addAdjacentSystem(system7);
        system5.addAdjacentSystem(system4);

        system6.addAdjacentSystem(system1);
        system6.addAdjacentSystem(system7);
        system6.addAdjacentSystem(system5);

        system7.addAdjacentSystem(system1);
        system7.addAdjacentSystem(system2);
        system7.addAdjacentSystem(system3);
        system7.addAdjacentSystem(system4);
        system7.addAdjacentSystem(system5);
        system7.addAdjacentSystem(system6);
    }

    public void makeMap(){
        java.lang.System.out.println("Game is setting up...");
        generateSystemAdjacency();
        java.lang.System.out.println("Game setup without error");
        java.lang.System.out.println();
    }


    public System System1() {
        return system1;
    }

    public System System2() {
        return system2;
    }

    public System System3() {
        return system3;
    }

    public System System4() {
        return system4;
    }

    public System System5() {
        return system5;
    }

    public System System6() {
        return system6;
    }

    public System System7() {
        return system7;
    }

    public List<System> getSystemList() {
        List<System> systemList = new ArrayList<>();

        systemList.add(system1);
        systemList.add(system2);
        systemList.add(system3);
        systemList.add(system4);
        systemList.add(system5);
        systemList.add(system6);
        systemList.add(system7);

        return systemList;
    }
}
