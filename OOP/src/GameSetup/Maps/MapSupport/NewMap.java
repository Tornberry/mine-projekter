package GameSetup.Maps.MapSupport;

public interface NewMap {
    Map map = new Map();

    void populateSystems();
    Map baseMap();
}
