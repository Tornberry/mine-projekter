package GameSetup.Support.Pickers;

import Models.Planet;
import Models.System;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PlanetPicker {
    private Planet Abyz = new Planet("Abyz", 3);
    private Planet Fria = new Planet("Fria", 2);
    private Planet Arinam = new Planet("Arinam", 1);
    private Planet Meer = new Planet("Meer", 0);
    private Planet Arnor = new Planet("Arnor", 2);
    private Planet Lor = new Planet("Lor", 1);
    private Planet Bereg = new Planet("Bereg", 3);
    private Planet Lirta_IV = new Planet("Lirta IV", 2);
    private Planet Centauri = new Planet("Centauri", 1);
    private Planet Gral = new Planet("Gral", 1);
    private Planet Coorneeq = new Planet("Coorneeq", 1);
    private Planet Resculon = new Planet("Resculon",2);
    private Planet Dal_Bootha = new Planet("Dal Bootha", 0);
    private Planet XXehan = new Planet("XXehan", 1);
    private Planet Lazar = new Planet("Lazar", 1);
    private Planet Sakulag = new Planet("Sakulag", 2);
    private Planet Lodor = new Planet("Lodor", 3);
    private Planet Mecatol_Rex = new Planet("Mecatol Rex", 1);
    private Planet Mehar_Xull = new Planet("Mehar Xull", 1);
    private Planet Mellon = new Planet("Mellon", 0);
    private Planet Zohbat = new Planet("Zohbat", 3);
    private Planet New_Albion = new Planet("New Albion", 1);
    private Planet Starpoint = new Planet("Starpoint", 3);
    private Planet Quann = new Planet("Quann", 2);
    private Planet Qucenn = new Planet("Qucen'n", 1);
    private Planet Rarron = new Planet("Rarron", 0);
    private Planet Saudor = new Planet("Saudor", 2);
    private Planet TarMann = new Planet("Tar'Mann", 1);
    private Planet TequRan = new Planet("Tequ'ran", 2);
    private Planet Torkan = new Planet("Torkan", 0);
    private Planet Thibah = new Planet("Thibah", 1);
    private Planet Vefut_II = new Planet("Vefut II", 2);
    private Planet Wellon = new Planet("Wellon", 1);
    private Planet Vega_Minor = new Planet("Vega Minor", 1);
    private Planet Vega_Major = new Planet("Vega Major", 1);
    private Planet Industrex = new Planet("Industrex", 1);
    private Planet Rigel_I = new Planet("Rigel I", 0);
    private Planet Rigel_II = new Planet("Rigel II", 0);
    private Planet Mirage = new Planet("Mirage", 1);

    private List<Planet> planetList = new ArrayList<>();

    private void addPlanetsToList(){
        planetList.add(Abyz);
        planetList.add(Fria);
        planetList.add(Arinam);
        planetList.add(Meer);
        planetList.add(Arnor);
        planetList.add(Lor);
        planetList.add(Bereg);
        planetList.add(Lirta_IV);
        planetList.add(Centauri);
        planetList.add(Gral);
        planetList.add(Coorneeq);
        planetList.add(Resculon);
        planetList.add(Dal_Bootha);
        planetList.add(XXehan);
        planetList.add(Lazar);
        planetList.add(Sakulag);
        planetList.add(Lodor);
        planetList.add(Mecatol_Rex);
        planetList.add(Mehar_Xull);
        planetList.add(Mellon);
        planetList.add(Zohbat);
        planetList.add(New_Albion);
        planetList.add(Starpoint);
        planetList.add(Quann);
        planetList.add(Qucenn);
        planetList.add(Rarron);
        planetList.add(Saudor);
        planetList.add(TarMann);
        planetList.add(TequRan);
        planetList.add(Torkan);
        planetList.add(Thibah);
        planetList.add(Vefut_II);
        planetList.add(Wellon);
        planetList.add(Vega_Minor);
        planetList.add(Vega_Major);
        planetList.add(Industrex);
        planetList.add(Rigel_I);
        planetList.add(Rigel_II);
        planetList.add(Mirage);
    }

    public Planet Abyz() {
        return Abyz;
    }

    public Planet Fria() {
        return Fria;
    }

    public Planet Arinam() {
        return Arinam;
    }

    public Planet Meer() {
        return Meer;
    }

    public Planet Arnor() {
        return Arnor;
    }

    public Planet Lor() {
        return Lor;
    }

    public Planet Bereg() {
        return Bereg;
    }

    public Planet Lirta_IV() {
        return Lirta_IV;
    }

    public Planet Centauri() {
        return Centauri;
    }

    public Planet Gral() {
        return Gral;
    }

    public Planet Coorneeq() {
        return Coorneeq;
    }

    public Planet Resculon() {
        return Resculon;
    }

    public Planet Dal_Bootha() {
        return Dal_Bootha;
    }

    public Planet XXehan() {
        return XXehan;
    }

    public Planet Lazar() {
        return Lazar;
    }

    public Planet Sakulag() {
        return Sakulag;
    }

    public Planet Lodor() {
        return Lodor;
    }

    public Planet Mecatol_Rex() {
        return Mecatol_Rex;
    }

    public Planet Mehar_Xull() {
        return Mehar_Xull;
    }

    public Planet Mellon() {
        return Mellon;
    }

    public Planet Zohbat() {
        return Zohbat;
    }

    public Planet New_Albion() {
        return New_Albion;
    }

    public Planet Starpoint() {
        return Starpoint;
    }

    public Planet Quann() {
        return Quann;
    }

    public Planet Qucenn() {
        return Qucenn;
    }

    public Planet Rarron() {
        return Rarron;
    }

    public Planet Saudor() {
        return Saudor;
    }

    public Planet TarMann() {
        return TarMann;
    }

    public Planet TequRan() {
        return TequRan;
    }

    public Planet Torkan() {
        return Torkan;
    }

    public Planet Thibah() {
        return Thibah;
    }

    public Planet Vefut_II() {
        return Vefut_II;
    }

    public Planet Wellon() {
        return Wellon;
    }

    public Planet Vega_Minor() {
        return Vega_Minor;
    }

    public Planet Vega_Major() {
        return Vega_Major;
    }

    public Planet Industrex() {
        return Industrex;
    }

    public Planet Rigel_I() {
        return Rigel_I;
    }

    public Planet Rigel_II() {
        return Rigel_II;
    }

    public Planet Mirage() {
        return Mirage;
    }

    public Planet RandomPlanet(){
        planetList.clear();
        addPlanetsToList();
        Random randomNumber = new Random();
        int randomRaceNumber = randomNumber.nextInt(37);

        if (!planetList.get(randomRaceNumber).isInGalaxy()){
            planetList.get(randomRaceNumber).setInGalaxy(true);
            return planetList.get(randomRaceNumber);
        } else {
            RandomPlanet();
            return null;
        }
    }
}
