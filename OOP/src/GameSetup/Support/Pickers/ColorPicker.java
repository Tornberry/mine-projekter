package GameSetup.Support.Pickers;

import java.awt.*;

public class ColorPicker {

    private String[] red = {"Red", "false"};
    private String[] blue = {"Blue", "false"};
    private String[] yellow = {"Yellow", "false"};
    private String[] green = {"Green", "false"};
    private String[] pink = {"Pink", "false"};
    private String[] black = {"Black", "false"};
    private String[] white = {"White", "false"};
    private String[] purple = {"Purple", "false"};

    public String pickRed() {
        red[1] = "true";
        return red[0];
    }

    public String pickBlue() {
        blue[1] = "true";
        return blue[0];
    }

    public String pickYellow() {
        yellow[1] = "true";
        return yellow[0];
    }

    public String pickGreen() {
        green[1] = "true";
        return green[0];
    }

    public String pickPink() {
        pink[1] = "true";
        return pink[0];
    }

    public String picketBlack() {
        black[1] = "true";
        return black[0];
    }

    public String pickWhite() {
        white[1] = "true";
        return white[0];
    }

    public String pickPurple() {
        purple[1] = "true";
        return purple[0];
    }
}
