package GameSetup.Support.Pickers;

import Models.Race;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RacePicker {

    private Race The_Barony_of_Letnev = new Race("The Barony of Letnev");
    private Race The_Emirates_of_Hacan = new Race("The Emirates of Hacan");
    private Race Federation_of_Sol = new Race("Federation of Sol");
    private Race The_L1Z1X_Mindnet = new Race("The L1Z1X Mindnet");
    private Race The_Mentak_Coalition = new Race("The Mentak Coalition");
    private Race The_Naalu_Collective = new Race("The Naalu Collective");
    private Race Sardakk_Norr = new Race("Sardakk N’orr");
    private Race The_Universities_of_JolNar = new Race("The Universities of Jol-Nar");
    private Race The_Xxcha_Kingdom = new Race("The Xxcha Kingdom");
    private Race The_Yssaril_Tribes = new Race("The Yssaril Tribes");
    private Race Arborec = new Race("Arborec");
    private Race Nekro_Virus = new Race("Nekro Virus");
    private Race The_Yin_Brotherhood = new Race("The Yin Brotherhood");
    private Race Clan_of_Saar = new Race("Clan of Saar");
    private Race Winnu = new Race("Winnu");
    private Race Embers_of_Muaat = new Race("Embers of Muaat");
    private Race Ghosts_of_Creuss = new Race("Ghosts of Creuss");

    List<Race> races = new ArrayList<>();

    public Race pickThe_Barony_of_Letnev() {
        The_Barony_of_Letnev.setPicked(true);
        return The_Barony_of_Letnev;
    }

    public Race pickThe_Emirates_of_Hacan() {
        The_Emirates_of_Hacan.setPicked(true);
        return The_Emirates_of_Hacan;
    }

    public Race pickFederation_of_Sol() {
        Federation_of_Sol.setPicked(true);
        return Federation_of_Sol;
    }

    public Race pickThe_L1Z1X_Mindnet() {
        The_L1Z1X_Mindnet.setPicked(true);
        return The_L1Z1X_Mindnet;
    }

    public Race pickThe_Mentak_Coalition() {
        The_Mentak_Coalition.setPicked(true);
        return The_Mentak_Coalition;
    }

    public Race pickThe_Naalu_Collective() {
        The_Naalu_Collective.setPicked(true);
        return The_Naalu_Collective;
    }

    public Race pickSardakk_Norr() {
        Sardakk_Norr.setPicked(true);
        return Sardakk_Norr;
    }

    public Race pickThe_Universities_of_JolNar() {
        The_Universities_of_JolNar.setPicked(true);
        return The_Universities_of_JolNar;
    }

    public Race pickThe_Xxcha_Kingdom() {
        The_Xxcha_Kingdom.setPicked(true);
        return The_Xxcha_Kingdom;
    }

    public Race pickThe_Yssaril_Tribes() {
        The_Yssaril_Tribes.setPicked(true);
        return The_Yssaril_Tribes;
    }

    public Race pickArborec() {
        Arborec.setPicked(true);
        return Arborec;
    }

    public Race pickNekro_Virus() {
        Nekro_Virus.setPicked(true);
        return Nekro_Virus;
    }

    public Race pickThe_Yin_Brotherhood() {
        The_Yin_Brotherhood.setPicked(true);
        return The_Yin_Brotherhood;
    }

    public Race pickClan_of_Saar() {
        Clan_of_Saar.setPicked(true);
        return Clan_of_Saar;
    }

    public Race pickWinnu() {
        Winnu.setPicked(true);
        return Winnu;
    }

    public Race pickEmbers_of_Muaat() {
        Embers_of_Muaat.setPicked(true);
        return Embers_of_Muaat;
    }

    public Race pickGhosts_of_Creuss() {
        Ghosts_of_Creuss.setPicked(true);
        return Ghosts_of_Creuss;
    }

    private void addRacesToList(){
        races.add(The_Barony_of_Letnev);
        races.add(The_Emirates_of_Hacan);
        races.add(Federation_of_Sol);
        races.add(The_L1Z1X_Mindnet);
        races.add(The_Mentak_Coalition);
        races.add(The_Naalu_Collective);
        races.add(Sardakk_Norr);
        races.add(The_Universities_of_JolNar);
        races.add(The_Xxcha_Kingdom);
        races.add(The_Yssaril_Tribes);
        races.add(Arborec);
        races.add(Nekro_Virus);
        races.add(The_Yin_Brotherhood);
        races.add(Clan_of_Saar);
        races.add(Winnu);
        races.add(Embers_of_Muaat);
        races.add(Ghosts_of_Creuss);
    }

    public Race pickRandomRace(){
        races.clear();
        addRacesToList();
        Random randomNumber = new Random();
        int randomRaceNumber = randomNumber.nextInt(16);

        if (!races.get(randomRaceNumber).isPicked()){
            races.get(randomRaceNumber).setPicked(true);
            return races.get(randomRaceNumber);
        } else {
            pickRandomRace();
            return null;
        }
    }
}
