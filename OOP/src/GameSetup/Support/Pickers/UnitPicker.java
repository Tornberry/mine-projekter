package GameSetup.Support.Pickers;

import Models.Units.Carrier;
import Models.Units.Cruiser;
import Models.Units.Destroyer;
import Models.Units.Dreadnought;

public class UnitPicker {
    String carrier = "Carrier";
    String cruiser = "Cruiser";
    String destroyer = "Destroyer";
    String dreadnought = "Dreadnought";

    public String Carrier() {
        return carrier;
    }

    public String Cruiser() {
        return cruiser;
    }

    public String Destroyer() {
        return destroyer;
    }

    public String Dreadnought() {
        return dreadnought;
    }
}
