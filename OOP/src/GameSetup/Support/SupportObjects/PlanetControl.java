package GameSetup.Support.SupportObjects;

import Models.Planet;
import Models.Player;

import java.util.ArrayList;
import java.util.List;

public class PlanetControl {
    private Player player;
    private List<Planet> planetList = new ArrayList<>();

    public PlanetControl(Player player) {
        this.player = player;
    }

    public void addPlanet(Planet planet){
        this.planetList.add(planet);
    }

    public Player getPlayer() {
        return player;
    }

    public List<Planet> getPlanetList() {
        return planetList;
    }
}
