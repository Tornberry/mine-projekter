package GameSetup;

import GameSetup.Maps.Opgave7Map;
import GameSetup.Maps.RandomMap;
import GameSetup.Maps.iDontKnow;
import GameSetup.Support.SupportObjects.PlanetControl;
import Models.Galaxy;
import Models.Planet;
import Models.Player;
import Models.System;
import Models.Units.Carrier;
import Models.Units.Cruiser;
import Models.Units.Destroyer;
import Models.Units.Dreadnought;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Game {
    public RandomMap gameMap = new RandomMap();

    private Galaxy Galaxy = new Galaxy();

    public Game() {
        SetupGame();
    }

    public void SetupGame(){
        gameMap.populateSystems();

        for (System system : gameMap.baseMap().getSystemList()) {
            Galaxy.addSystem(system);
        }

        galaxyIsLegal();
    }

    public void addUnitToSystem(String unit, System system, Player owner){
        if (unit.equals("Carrier")){
            system.addCarrier(owner);
            java.lang.System.out.println("Carrier added successfully to system " + system.getName());
        } else if (unit.equals("Cruiser")) {
            system.addCruiser(owner);
            java.lang.System.out.println("Cruiser added successfully to system " + system.getName());
        } else if (unit.equals("Destroyer")) {
            system.addDestroyer(owner);
            java.lang.System.out.println("Destroyer added successfully to system " + system.getName());
        } else if (unit.equals("Dreadnought")) {
            system.addDreadnought(owner);
            java.lang.System.out.println("Dreadnought added successfully to system " + system.getName());
        }
    }

    public void savePlanetControlToFile(List<Player> playerList) throws IOException {
        Player systemOwner = null;
        List<PlanetControl> planetControlList = new ArrayList<>();

        for (Player player : playerList){
            planetControlList.add(new PlanetControl(player));
        }

        File fout = new File("control.txt");
        FileOutputStream fos = new FileOutputStream(fout);
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

        outerLoop:
        for (System system : Galaxy().getSystems()){
            for (Carrier carrier : system.getCarriers()){
                if (systemOwner == null){
                    systemOwner = carrier.getOwner();
                } else {
                    if (!(systemOwner == carrier.getOwner())){
                        break outerLoop;
                    }
                }
            }
            for (Cruiser cruiser : system.getCruisers()){
                if (systemOwner == null){
                    systemOwner = cruiser.getOwner();
                } else {
                    if (!(systemOwner == cruiser.getOwner())){
                        break outerLoop;
                    }
                }
            }
            for (Destroyer destroyer : system.getDestroyers()){
                if (systemOwner == null){
                    systemOwner = destroyer.getOwner();
                } else {
                    if (!(systemOwner == destroyer.getOwner())){
                        break outerLoop;
                    }
                }
            }
            for (Dreadnought dreadnought : system.getDreadnoughts()){
                if (systemOwner == null){
                    systemOwner = dreadnought.getOwner();
                } else {
                    if (!(systemOwner == dreadnought.getOwner())){
                        break outerLoop;
                    }
                }
            }

            if (!(systemOwner == null)){
                for (PlanetControl planetControl : planetControlList){
                    if (systemOwner == planetControl.getPlayer()){
                        for (Planet planet : system.getPlanets()){
                            planetControl.addPlanet(planet);
                        }
                    }
                }
            }

            systemOwner = null;
        }

        for (PlanetControl planetControl : planetControlList){
            bw.write(planetControl.getPlayer().getPlayerName() + "(" + planetControl.getPlayer().getRace().getRaceName() + ")" + " have control over the following planets:");
            bw.newLine();
            for (Planet planet : planetControl.getPlanetList()){
                bw.write("-" + planet.getName());
                bw.newLine();
            }
        }
        java.lang.System.out.println("Printing of planet control completed");
        bw.close();
    }

    private void galaxyIsLegal(){
        boolean checkCenter = true;
        boolean checkPlanetOnlyOnePlace = true;
        boolean checkMaxPlanets = true;
        boolean checkAdjacency = true;

        List<Planet> planetList = new ArrayList<>();

        java.lang.System.out.println("Approving galaxy for correct setup...");


        if (!(Galaxy.getSystems().get(6).getPlanets().get(0).getName().equals("Mecatol Rex") && Galaxy.getSystems().get(6).getPlanets().size() == 1)) {
            checkCenter = false;
        }

        for (System system : Galaxy.getSystems()){
            for (Planet planet : system.getPlanets()){
                if (planetList.contains(planet)){
                    checkPlanetOnlyOnePlace = false;
                } else {
                    planetList.add(planet);
                }
            }
        }

        for (System system : Galaxy.getSystems()) {
            if (!(system.getPlanets().size() <= 3)) {
                checkMaxPlanets = false;
            }
        }

        for (System system : Galaxy.getSystems()) {
            for (System adjacentSystem : system.getAdjacentSystems()) {
                if (!(adjacentSystem.getAdjacentSystems().contains(system))) {
                    checkAdjacency = false;
                }
            }
        }

        if (!(checkCenter && checkMaxPlanets && checkAdjacency && checkPlanetOnlyOnePlace)) {
            throw new IllegalStateException();
        } else {
            java.lang.System.out.println("Galaxy approved");
        }

    }

    private int calculateHitsFromCarriers(List<Carrier> carrierList) {
        Random dice = new Random();
        int hits = 0;

        for (Carrier carrier : carrierList) {
            if (carrier.getCombatValue() <= dice.nextInt(10) + 1) {
                hits += 1;
            }
        }
        return hits;
    }

    private int calculateHitsFromCruisers(List<Cruiser> cruiserList) {
        Random dice = new Random();
        int hits = 0;

        for (Cruiser cruiser : cruiserList) {
            if (cruiser.getCombatValue() <= dice.nextInt(10) + 1) {
                hits += 1;
            }
        }
        return hits;
    }

    private int calculateHitsFromDestroyers(List<Destroyer> destroyerList) {
        Random dice = new Random();
        int hits = 0;

        for (Destroyer destroyer: destroyerList) {
            if (destroyer.getCombatValue() <= dice.nextInt(10) + 1) {
                hits += 1;
            }
        }
        return hits;
    }

    private int calculateHitsFromDreadnoughts(List<Dreadnought> dreadnoughtList) {
        Random dice = new Random();
        int hits = 0;

        for (Dreadnought dreadnought: dreadnoughtList) {
            if (dreadnought.getCombatValue() <= dice.nextInt(10) + 1) {
                hits += 1;
            }
        }
        return hits;
    }

    public Player resolveSpaceBattle(System system, Player player1, Player player2){

        List<Carrier> player1Carriers = new ArrayList<>();
        List<Carrier> player2Carriers = new ArrayList<>();

        List<Cruiser> player1Cruisers = new ArrayList<>();
        List<Cruiser> player2Cruisers = new ArrayList<>();

        List<Destroyer> player1Destroyers = new ArrayList<>();
        List<Destroyer> player2Destroyers = new ArrayList<>();

        List<Dreadnought> player1Dreadnoughts = new ArrayList<>();
        List<Dreadnought> player2Dreadnoughts = new ArrayList<>();

        int player1Spaceships = 0;
        int player2Spaceships = 0;
        int player1Hits = 0;
        int player2Hits = 0;

        // Populate spaceship lists ----------------------
        for (Carrier carrier : system.getCarriers()){
            if (carrier.getOwner() == player1){
                player1Carriers.add(carrier);
            } else if(carrier.getOwner() == player2){
                player2Carriers.add(carrier);
            }
        }

        for (Cruiser cruiser : system.getCruisers()){
            if (cruiser.getOwner() == player1){
                player1Cruisers.add(cruiser);
            } else if(cruiser.getOwner() == player2){
                player2Cruisers.add(cruiser);
            }
        }

        for (Destroyer destroyer : system.getDestroyers()){
            if (destroyer.getOwner() == player1){
                player1Destroyers.add(destroyer);
            } else if(destroyer.getOwner() == player2){
                player2Destroyers.add(destroyer);
            }
        }

        for (Dreadnought dreadnought : system.getDreadnoughts()){
            if (dreadnought.getOwner() == player1){
                player1Dreadnoughts.add(dreadnought);
            } else if(dreadnought.getOwner() == player2){
                player2Dreadnoughts.add(dreadnought);
            }
        }
        // ----------------------------------------------

        player1Spaceships = player1Carriers.size() + player1Cruisers.size() + player1Destroyers.size() + player1Dreadnoughts.size();
        player2Spaceships = player2Carriers.size() + player2Cruisers.size() + player2Destroyers.size() + player2Dreadnoughts.size();

        // Determin hits for both players ---------------
        player1Hits += calculateHitsFromCarriers(player1Carriers);
        player2Hits += calculateHitsFromCarriers(player2Carriers);

        player1Hits += calculateHitsFromCruisers(player1Cruisers);
        player2Hits += calculateHitsFromCruisers(player2Cruisers);

        player1Hits += calculateHitsFromDestroyers(player1Destroyers);
        player2Hits += calculateHitsFromDestroyers(player2Destroyers);

        player1Hits += calculateHitsFromDreadnoughts(player1Dreadnoughts);
        player2Hits += calculateHitsFromDreadnoughts(player2Dreadnoughts);

        // ------------------------------------------------

        // Remove ships based on hits ---------------------
        int iterationNumber = 0;
        while (player1Spaceships > 0 && player2Spaceships > 0){

            if (iterationNumber == 0){
                player1Hits += calculateHitsFromCarriers(player1Carriers);
                player2Hits += calculateHitsFromCarriers(player2Carriers);

                player1Hits += calculateHitsFromCruisers(player1Cruisers);
                player2Hits += calculateHitsFromCruisers(player2Cruisers);

                player1Hits += calculateHitsFromDestroyers(player1Destroyers);
                player2Hits += calculateHitsFromDestroyers(player2Destroyers);

                player1Hits += calculateHitsFromDreadnoughts(player1Dreadnoughts);
                player2Hits += calculateHitsFromDreadnoughts(player2Dreadnoughts);
            }

            for (Destroyer destroyer : player1Destroyers){
                if (player2Hits > 0){
                    system.removeDestroyerByPlayer(player1);
                    player1Spaceships -= 1;
                    player2Hits -= 1;
                }
            }

            for (Destroyer destroyer : player2Destroyers){
                if (player1Hits > 0){
                    system.removeDestroyerByPlayer(player2);
                    player2Spaceships -= 1;
                    player1Hits -= 1;
                }
            }

            for (Cruiser cruiser : player1Cruisers){
                if (player2Hits > 0){
                    system.removeCruiserByPlayer(player1);
                    player1Spaceships -= 1;
                    player2Hits -= 1;
                }
            }

            for (Cruiser cruiser : player2Cruisers){
                if (player1Hits > 0){
                    system.removeCruiserByPlayer(player2);
                    player2Spaceships -= 1;
                    player1Hits -= 1;
                }
            }

            for (Carrier carrier : player1Carriers){
                if (player2Hits > 0){
                    system.removeCarrierByPlayer(player1);
                    player1Spaceships -= 1;
                    player2Hits -= 1;
                }
            }

            for (Carrier carrier : player2Carriers){
                if (player1Hits > 0){
                    system.removeCarrierByPlayer(player2);
                    player2Spaceships -= 1;
                    player1Hits -= 1;
                }
            }

            for (Dreadnought dreadnought : player1Dreadnoughts){
                if (player2Hits > 0){
                    system.removeDreadnoughtByPlayer(player1);
                    player1Spaceships -= 1;
                    player2Hits -= 1;
                }
            }

            for (Dreadnought dreadnought : player2Dreadnoughts){
                if (player1Hits > 0){
                    system.removeDreadnoughtByPlayer(player2);
                    player2Spaceships -= 1;
                    player1Hits -= 1;
                }
            }
            // ------------------------------------------------

            iterationNumber += 1;
        }


        java.lang.System.out.println(player1Spaceships);
        java.lang.System.out.println(player2Spaceships);
        return null;
    }

    public Galaxy Galaxy() {
        return Galaxy;
    }


}
