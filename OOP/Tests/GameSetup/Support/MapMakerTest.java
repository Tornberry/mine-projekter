package GameSetup.Support;

import GameSetup.Game;
import GameSetup.Support.Pickers.ColorPicker;
import GameSetup.Support.Pickers.RacePicker;
import Models.Player;
import Models.System;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class MapMakerTest {

    //MapMaker mapMaker = new MapMaker();
    @Test
    public void systemInitializationTest() {
//        assertEquals("North",mapMaker.system1.getName());
//        assertEquals("North East", mapMaker.system2.getName());
//        assertEquals("South East", mapMaker.system3.getName());
//        assertEquals("South", mapMaker.system4.getName());
//        assertEquals("South West", mapMaker.system5.getName());
//        assertEquals("North West", mapMaker.system6.getName());
//        assertEquals("Center", mapMaker.system7.getName());
    }

    @Test
    public void populateSystemTest() {
//        mapMaker.populateSystems();
//
//        //En test for system 1's planeter. Tester planets navn, resource production og hvor mange planeter der er i systemet
//        assertEquals("Vega Minor", mapMaker.system1.getPlanets().get(0).getName());
//        assertEquals(1, mapMaker.system1.getPlanets().get(0).getResourceProduction());
//        assertEquals("Vega Major", mapMaker.system1.getPlanets().get(1).getName());
//        assertEquals(1, mapMaker.system1.getPlanets().get(1).getResourceProduction());
//        assertEquals(2, mapMaker.system1.getPlanets().size());
//
//        //En test for system 2's planeter. Tester planets navn, resource production og hvor mange planeter der er i systemet
//        assertEquals(0, mapMaker.system2.getPlanets().size());
//
//        //En test for system 3's planeter. Tester planets navn, resource production og hvor mange planeter der er i systemet
//        assertEquals("Industrex", mapMaker.system3.getPlanets().get(0).getName());
//        assertEquals(2, mapMaker.system3.getPlanets().get(0).getResourceProduction());
//        assertEquals(1, mapMaker.system3.getPlanets().size());
//
//        //En test for system 4's planeter. Tester planets navn, resource production og hvor mange planeter der er i systemet
//        assertEquals("Rigel I", mapMaker.system4.getPlanets().get(0).getName());
//        assertEquals(0, mapMaker.system4.getPlanets().get(0).getResourceProduction());
//        assertEquals("Rigel II", mapMaker.system4.getPlanets().get(1).getName());
//        assertEquals(0, mapMaker.system4.getPlanets().get(1).getResourceProduction());
//        assertEquals(2, mapMaker.system4.getPlanets().size());
//
//        //En test for system 5's planeter. Tester planets navn, resource production og hvor mange planeter der er i systemet
//        assertEquals(0, mapMaker.system5.getPlanets().size());
//
//        //En test for system 6's planeter. Tester planets navn, resource production og hvor mange planeter der er i systemet
//        assertEquals("Mirage", mapMaker.system6.getPlanets().get(0).getName());
//        assertEquals(1, mapMaker.system6.getPlanets().get(0).getResourceProduction());
//        assertEquals(1, mapMaker.system6.getPlanets().size());
//
//        //En test for system 7's planeter. Tester planets navn, resource production og hvor mange planeter der er i systemet
//        assertEquals("Mecatol Rex", mapMaker.system7.getPlanets().get(0).getName());
//        assertEquals(1, mapMaker.system7.getPlanets().get(0).getResourceProduction());
//        assertEquals(1, mapMaker.system7.getPlanets().size());


    }

    @Test
    public void systemAdjacencyTest() {
//        mapMaker.generateSystemAdjacency();
//
//        //Test om alle korrekte systemer i system1 er tilstede
//        assertTrue("Nogle af de tilstødende systemer er ikke tilstede for system1, eller der er for mange tilstødenden systemer", mapMaker.system1.getAdjacentSystems().containsAll(new ArrayList<System>() {{
//            add(mapMaker.getSystem2());
//            add(mapMaker.getSystem7());
//            add(mapMaker.getSystem6());
//        }}));
//
//        //Test om alle korrekte systemer i system2 er tilstede
//        assertTrue("Nogle af de tilstødende systemer er ikke tilstede for system2, eller der er for mange tilstødenden systemer", mapMaker.system2.getAdjacentSystems().containsAll(new ArrayList<System>() {{
//            add(mapMaker.getSystem1());
//            add(mapMaker.getSystem7());
//            add(mapMaker.getSystem3());
//        }}));
//
//        //Test om alle korrekte systemer i system3 er tilstede
//        assertTrue("Nogle af de tilstødende systemer er ikke tilstede for system3, eller der er for mange tilstødenden systemer", mapMaker.system3.getAdjacentSystems().containsAll(new ArrayList<System>() {{
//            add(mapMaker.getSystem2());
//            add(mapMaker.getSystem7());
//            add(mapMaker.getSystem4());
//        }}));
//
//        //Test om alle korrekte systemer i system4 er tilstede
//        assertTrue("Nogle af de tilstødende systemer er ikke tilstede for system4, eller der er for mange tilstødenden systemer", mapMaker.system4.getAdjacentSystems().containsAll(new ArrayList<System>() {{
//            add(mapMaker.getSystem5());
//            add(mapMaker.getSystem7());
//            add(mapMaker.getSystem3());
//        }}));
//
//        //Test om alle korrekte systemer i system5 er tilstede
//        assertTrue("Nogle af de tilstødende systemer er ikke tilstede for system5, eller der er for mange tilstødenden systemer", mapMaker.system5.getAdjacentSystems().containsAll(new ArrayList<System>() {{
//            add(mapMaker.getSystem6());
//            add(mapMaker.getSystem7());
//            add(mapMaker.getSystem4());
//        }}));
//
//        //Test om alle korrekte systemer i system6 er tilstede
//        assertTrue("Nogle af de tilstødende systemer er ikke tilstede for system6, eller der er for mange tilstødenden systemer", mapMaker.system6.getAdjacentSystems().containsAll(new ArrayList<System>() {{
//            add(mapMaker.getSystem1());
//            add(mapMaker.getSystem7());
//            add(mapMaker.getSystem5());
//        }}));
//
//        //Test om alle korrekte systemer i system7 er tilstede
//        assertTrue("Nogle af de tilstødende systemer er ikke tilstede for system7, eller der er for mange tilstødenden systemer", mapMaker.system7.getAdjacentSystems().containsAll(new ArrayList<System>() {{
//            add(mapMaker.getSystem1());
//            add(mapMaker.getSystem2());
//            add(mapMaker.getSystem3());
//            add(mapMaker.getSystem4());
//            add(mapMaker.getSystem5());
//            add(mapMaker.getSystem6());
//        }}));
    }

    @Test
    public void unitAddingTest() {

//        RacePicker racePicker = new RacePicker();
//        ColorPicker colorPicker = new ColorPicker();
//
//        Player redPlayer = new Player("Simon", racePicker.pickRandomRace(), colorPicker.pickRed());
//        Player bluePlayer = new Player("Lasse", racePicker.pickRandomRace(), colorPicker.pickBlue());
//
//        Game game = new Game();
//        game.SetupGame();
//
//        game.addUnitToSystem(game.unitPicker.Dreadnought(), game.Center(),bluePlayer);
//        game.addUnitToSystem(game.unitPicker.Dreadnought(), game.Center(),bluePlayer);
//        game.addUnitToSystem(game.unitPicker.Destroyer(),game.Center(), bluePlayer);
//
//        game.addUnitToSystem(game.unitPicker.Cruiser(), game.North(), redPlayer);
//        game.addUnitToSystem(game.unitPicker.Cruiser(), game.North(), redPlayer);
//        game.addUnitToSystem(game.unitPicker.Carrier(), game.North(), redPlayer);
//
//        assertEquals(2, game.Center().getDreadnoughts().size());
//        assertEquals(1, game.Center().getDestroyers().size());
//        assertEquals(2, game.North().getCruisers().size());
//        assertEquals(1, game.North().getCarriers().size());
    }
}