var modal = $('#welcomeModal');

$( document ).ready(function() {
    var collSetup = $('#collectionstate').val();

    if (collSetup == 0){

        modal.modal('show');

        console.log(modal);

        $.ajax({
            type: 'POST',
            url: '/user/setupcollection',
            data: "SetupCollection",
            success: function(){
                alert("Your collection is now setup, congratulations! Go to 'My collection' and see your empty collection!");
                window.location.reload();
            },
            error: function(){
            }
        });
    }
});
