var button = $('#amountAcceptButton');
var pc;
var amount;
var ownedOne = $('#ownedOne');
var ownedZero = $('#ownedZero');

$("#menu").metisMenu();

function getPC(id){
    pc = id;
    $('#setAmountTitle').html('Set amount owned for ' + pc);
    $('#setAmount').modal('show');

}

button.click(function(){
    amount = $('#amount').val();
    $.ajax({
        type: 'GET',
        url: '/carddatabase/editAmount',
        data: jQuery.param({ pc: pc, amount : amount}),
        success: function(){
           changeAmount();
        },
        error: function(){
            alert("error");
        }
    });
});

$.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
        return null;
    }
    else{
        return decodeURI(results[1]) || 0;
    }
}

ownedOne.click(function(){
    var set = new URL(window.location.href);
    var newSet = set.searchParams.get("set");
    console.log(newSet);
    $.ajax({
        type: 'GET',
        url: '/carddatabase/setAmountOne',
        data: jQuery.param({set: newSet}),
        success: function(){
            successReload();
        },
        error: function(){
            alert("error");
        }
    });
});

ownedZero.click(function(){
    $.ajax({
        type: 'GET',
        url: '/carddatabase/setAmountZero',
        data: jQuery.param({ set: $.urlParam('set')}),
        success: function(){
            successReload();
        },
        error: function(){
            alert("error");
        }
    });
});

function changeAmount(){
    var label = $('#'+pc+'label');
    var picture = $('#'+pc+'image');

    if (amount == 0){
        label.css("color", "red");
        label.text('Owned: ' + amount);
        picture.css("filter", "grayscale(100%)");
        $('#setAmount').modal('hide');
    } else {
        label.css("color", "green");
        label.text('Owned: ' + amount);
        picture.css("filter", "");
        $('#setAmount').modal('hide');
    }
}

function addOne(id){
    pc = id;

    $.ajax({
        type: 'POST',
        url: '/carddatabase/addOrSubtract',
        data: {PCID: id, amountType: 1},
        success: function(){
            changeAmountPlus();
        },
        error: function(){
            alert("Error, if this happens again please contact the development team");
        }
    });
}

function subtractOne(id){
    pc = id;

    $.ajax({
        type: 'POST',
        url: '/carddatabase/addOrSubtract',
        data: {PCID: id, amountType: 2},
        success: function(){
            changeAmountMinus();
        },
        error: function(){
            alert("Error, if this happens again please contact the development team");
        }
    });
}

function changeAmountPlus(){
    var label = $('#'+pc+'label');
    var picture = $('#'+pc+'image');

    var currentAmount = parseInt(label.text().split(': ')[1]);
    var oneAddedToAmount = currentAmount + 1;

    if (oneAddedToAmount == 0){
        label.css("color", "red");
        label.text('Owned: ' + oneAddedToAmount);
        picture.css("filter", "grayscale(100%)");
        $('#setAmount').modal('hide');
    } else {
        label.css("color", "green");
        label.text('Owned: ' + oneAddedToAmount);
        picture.css("filter", "");
        $('#setAmount').modal('hide');
    }
}

function changeAmountMinus(){
    var label = $('#'+pc+'label');
    var picture = $('#'+pc+'image');

    var currentAmount = parseInt(label.text().split(': ')[1]);
    var oneSubtractedToAmount = currentAmount - 1;

    if (oneSubtractedToAmount == 0){
        label.css("color", "red");
        label.text('Owned: ' + oneSubtractedToAmount);
        picture.css("filter", "grayscale(100%)");
        $('#setAmount').modal('hide');
    } else if(oneSubtractedToAmount > 0) {
        label.css("color", "green");
        label.text('Owned: ' + oneSubtractedToAmount);
        picture.css("filter", "");
        $('#setAmount').modal('hide');
    } else if (oneSubtractedToAmount < 0){
        alert("You cannot have a negative amount of cards")
    }
}

function successReload(){
    alert("Success, click to reload page");
    location.reload();
}


