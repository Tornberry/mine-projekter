
function AcceptRequest(id) {

    var formData = {
      id: id
    };

    $.ajax({
        type: 'POST',
        url: '/userNotifications/acceptFriendRequest',
        data: formData,
        success: function(){
            window.location.reload();
        },
        error: function(){
            alert("Error, could not accept friend request");
        }
    });

}

function DeclineRequest(id) {

    var formData = {
        id: id
    };

    $.ajax({
        type: 'POST',
        url: '/userNotifications/declineFriendRequest',
        data: formData,
        success: function(){
            window.location.reload();
        },
        error: function(){
            alert("Error, could not decline friend request");
        }
    });

}

function RemoveFriend(username){
    var formData = {
        username: username
    };

    $.ajax({
        type: 'POST',
        url: '/userNotifications/removeFriend',
        data: formData,
        success: function(){
            window.location.reload();
        },
        error: function(){
            alert("Error, could not remove friend");
        }
    });
}