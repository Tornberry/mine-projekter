var bannerUploadButton = $('#bannerUploadButton');
var avatarUploadButton = $('#avatarUploadButton');

bannerUploadButton.click(function(){
    var formData = new FormData($('#bannerForm')[0]);
    $.ajax({
        type: 'POST',
        url: '/myprofile/settings/bannerUpload',
        data: formData,
        success: function(){
            alert("Banner changed successfully!");
            window.location.reload();
        },
        error: function(){
            alert("Error uploading banner, try again. If this happens multiple times, please contact our developer team");
        },
        cache: false,
        contentType: false,
        processData: false
    });
});

avatarUploadButton.click(function(){
    var formData = new FormData($('#avatarForm')[0]);
    $.ajax({
        type: 'POST',
        url: '/myprofile/settings/avatarUpload',
        data: formData,
        success: function(){
            alert("Avatar changed successfully!");
            window.location.reload();
        },
        error: function(){
            alert("Error uploading avatar, try again. If this happens multiple times, please contact our developer team");
        },
        cache: false,
        contentType: false,
        processData: false
    });
});