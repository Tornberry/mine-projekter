var displayName = document.getElementById('displayName');
var joinDate = document.getElementById('joinDate');
var hiddenDisplayName = document.getElementById('hiddenUsername');
var hiddenJoinDate = document.getElementById('hiddenJoinDate');

var profileAvatar = document.getElementById('profileAvatar');
var profileBanner = document.getElementById('profileBanner');
var hiddenProfileAvatar = document.getElementById('hiddenAvatarUrl');
var hiddenProfileBanner = document.getElementById('hiddenBannerUrl');

var theirProfile = document.getElementById('theirProfile');
var setStatistics = document.getElementById('setStatistics');
var theirFriends = document.getElementById('seeTheirFriends');

var friendStatus = document.getElementById('hiddenFriendStatus');
var sendFriendRequestButton = $('#sendFriendRequestButton');
var friendRequestLabel = $('#friendRequestLabel');
var friendsTrueFalse = $('#hiddenFriendsTrueFalse');
var friendRequestSideBarButton = $('#friendRequestButton');
var isMe = $('#hiddenIsMe');

$(document).ready(function () {

    if(friendsTrueFalse.text() == "false" && isMe.text() == "false"){
        if (friendStatus.innerHTML == "0"){
            sendFriendRequestButton.css("color", "green");
            sendFriendRequestButton.html("<i class=\"fas fa-user-plus\"></i> Send friend request");
        } else if(friendStatus.innerHTML == "1") {
            sendFriendRequestButton.css("color", "DodgerBlue");
            sendFriendRequestButton.html("<i class=\"fas fa-user-clock\"></i> Friend request sent");
        }
    } else {
        if (isMe.text() == "true"){
            window.location.href = "/myprofile";
        }  else {
            friendRequestSideBarButton.css("display", "none");
        }
    }


    // header
    displayName.innerHTML = hiddenDisplayName.innerHTML;
    joinDate.innerHTML = hiddenJoinDate.innerHTML;
    profileAvatar.src = hiddenProfileAvatar.innerHTML;
    profileBanner.src = hiddenProfileBanner.innerHTML;

    // sidebar
    theirProfile.href = "/profile/" + displayName.innerHTML;
    setStatistics.href = "/profile/" + displayName.innerHTML + "/setStatistics";
    theirFriends.href = "/profile/" + displayName.innerHTML + "/friends";
});

sendFriendRequestButton.click(function () {
    var formData = {
        type: 2,
        reciever: hiddenDisplayName.innerHTML
    };

    $.ajax({
        type: 'POST',
        url: '/userNotifications/makeFriendRequest',
        data: formData,
        success: function(){
            window.location.reload();
        },
        error: function(){
            alert("Error, could not send friend request");
        }
    });
});


