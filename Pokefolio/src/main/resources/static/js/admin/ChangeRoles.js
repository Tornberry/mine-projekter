var AddRoleButton = $('#addRoleButton');

AddRoleButton.click(function(){
    $.ajax({
        type: 'GET',
        url: '/admin/addrole',
        data: jQuery.param({ username: $.urlParam('username'), role: $('#roleSelector').val()}),
        success: function(){
            //alert($('#roleSelector').val() + ' is now added to user: ' + $.urlParam('username') + ', click to reload page');
            location.reload();
        },
        error: function(){
            alert("Error, could not delete user. Report error to a developer");
        }
    });
});

function removeRole(id){
    $.ajax({
        type: 'GET',
        url: '/admin/removerole',
        data: jQuery.param({ username: $.urlParam('username'), role: id}),
        success: function(){
            //alert(id + ' is now removed from user: ' + $.urlParam('username') + ', click to reload page');
            location.reload();
        },
        error: function(){
            alert("Error, could not delete role. Report error to a developer");
        }
    });
}