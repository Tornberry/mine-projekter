var newPassLink = $('#sendPasswordLink');
var changeRolesLink = $('#changeRolesLink');
var username;
var acceptDeleteButton = $('#acceptDelete');

newPassLink.click(function(){
    alert("This feature is not implemented yet, sorry :(");
});

function changeRoles(usernameClicked)
{
    window.location.href = '/admin/changeroles?username=' + usernameClicked;
}

function deleteUserDialog(usernameClicked){
    username = usernameClicked;
    $('#userDeleteModalBody').html('Are you sure you wanna delete user ' + username + '?');
    $('#deleteUserModal').modal('show');
}

acceptDeleteButton.click(function(){
    $.ajax({
        type: 'GET',
        url: '/admin/deleteuser',
        data: jQuery.param({ username: username}),
        success: function(){
            //alert(username + ' is now deleted! Click to reload page');
            location.reload();
        },
        error: function(){
            alert("Error, could not delete user. Report error to a developer");
        }
    });
});