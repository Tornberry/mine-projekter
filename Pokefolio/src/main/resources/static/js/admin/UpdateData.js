var updateCardsAndSetsButton = $('#updateCardsAndSetsButton');
var updateCardsAndSetsYes = $('#updateCardsAndSetsYes');
var localUpdateCardsAndSets = $('#updateCardsAndSetsButtonLocal');

updateCardsAndSetsButton.click(function(){
    $('#updateCardsAndSetsModal').modal('show');
});

updateCardsAndSetsYes.click(function(){
    $('#updateCardsAndSetsModal').modal('hide');
    $.ajax({
        type: 'GET',
        url: '/data/updateData',
        data: "something",
        success: function(){
            alert("Everything has been uploaded!")
            location.reload();
        },
        error: function(){
            alert("Error, could not update data. Report error to a developer");
        }
    });
});

localUpdateCardsAndSets.click(function(){
    $.ajax({
        type: 'GET',
        url: '/data/updateDataLocal',
        data: "something",
        success: function(){
            alert("Everything has been uploaded locally!")
            location.reload();
        },
        error: function(){
            alert("Error, could not update data. Report error to a developer");
        }
    });
});

$.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
        return null;
    }
    else{
        return decodeURI(results[1]) || 0;
    }
}