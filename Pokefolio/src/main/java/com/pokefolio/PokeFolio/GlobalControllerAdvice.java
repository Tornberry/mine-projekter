package com.pokefolio.PokeFolio;

import com.pokefolio.PokeFolio.Models.Notification;
import com.pokefolio.PokeFolio.Models.user.User;
import com.pokefolio.PokeFolio.Services.UserService;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@ControllerAdvice(basePackages = {"com.pokefolio.PokeFolio.Controllers.admin","com.pokefolio.PokeFolio.Controllers.cards","com.pokefolio.PokeFolio.Controllers.data", "com.pokefolio.PokeFolio.Controllers.profile", "com.pokefolio.PokeFolio.Controllers.home"})
public class GlobalControllerAdvice {

    private UserService userService;

    public GlobalControllerAdvice(UserService userService) {
        this.userService = userService;
    }

    @ModelAttribute("currentUser")
    public User addAttributesCurrUser(Principal principal, Model model){

        return userService.findByUsername(principal.getName());

    }

    @ModelAttribute("currentUserNotifications")
    public List<Notification> addAttributesCurrUserNotifications(Principal principal, Model model){
        User currentUser = userService.findByUsername(principal.getName());
        List<Notification> unreadNotifications = new ArrayList<>();

        for (Notification notification : currentUser.getNotifications()){
            if (notification.getReadStatus() == 0){
                unreadNotifications.add(notification);
            }
        }

        return unreadNotifications;

    }

    @ModelAttribute("currentVersion")
    public String addAttributesCurrVersion(Principal principal, Model model){

        return "Alpha 1.1";

    }
}
