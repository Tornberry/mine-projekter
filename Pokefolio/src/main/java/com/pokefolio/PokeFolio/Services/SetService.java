package com.pokefolio.PokeFolio.Services;

import com.pokefolio.PokeFolio.Models.CardSet;
import com.pokefolio.PokeFolio.Repositories.setRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class SetService {

    private setRepository setRepository;

    @Autowired
    public SetService(com.pokefolio.PokeFolio.Repositories.setRepository setRepository) {
        this.setRepository = setRepository;
    }


    public void clearSets() {
        setRepository.deleteAll();
    }

    public void saveSet(CardSet cardSet) {
        setRepository.save(cardSet);
    }

    public List<CardSet> findBySeries(String series) {
        return setRepository.findBySeries(series);
    }

    public CardSet findByName(String name) {
        return setRepository.findByName(name);
    }

    public Iterable<CardSet> findAll() {
        return setRepository.findAll();
    }

    public void saveAll(List<CardSet> sets){
        setRepository.saveAll(sets);
    }
}
