package com.pokefolio.PokeFolio.Services;

import com.pokefolio.PokeFolio.Models.Card;
import com.pokefolio.PokeFolio.Models.user.User;
import com.pokefolio.PokeFolio.Models.user.collection.PersonalCard;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PersonalCardService {

    private com.pokefolio.PokeFolio.Repositories.personalCardRepository personalCardRepository;
    private UserService userService;
    private CardService cardService;

    public PersonalCardService(com.pokefolio.PokeFolio.Repositories.personalCardRepository personalCardRepository, UserService userService, CardService cardService) {
        this.personalCardRepository = personalCardRepository;
        this.userService = userService;
        this.cardService = cardService;
    }

    public void saveOrUpdate(PersonalCard personalCard){
        personalCardRepository.save(personalCard);
    }

    public void setupCollection(User user){

        List<PersonalCard> newCollection = new ArrayList<>();

        for (Card card : cardService.getAllCards()){
            PersonalCard personalCard = new PersonalCard(userService.findByUsername(user.getUsername()), card, 0, card.getCardSet());
            newCollection.add(personalCard);
        }

        user.setCollection(newCollection);
        userService.saveOrUpdate(user);
    }

    public Iterable<PersonalCard> getAllPersonalCardsByUserAndFromSet(User user, String fromSet){
        return personalCardRepository.findByUserAndFromSet(user, fromSet);
    }

    public PersonalCard findById(int id){
        return personalCardRepository.findById(id);
    }

    public Iterable<PersonalCard> findAll(){
        return personalCardRepository.findAll();
    }

    public void clearPC(){
        personalCardRepository.deleteAll();
    }
}
