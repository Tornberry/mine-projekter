package com.pokefolio.PokeFolio.Services;

import com.pokefolio.PokeFolio.Models.user.FriendRequest;
import com.pokefolio.PokeFolio.Repositories.friendRequestRepository;
import org.springframework.stereotype.Service;

@Service
public class FriendRequestService {

    private friendRequestRepository friendRequestRepository;

    public FriendRequestService(friendRequestRepository friendRequestRepository) {
        this.friendRequestRepository = friendRequestRepository;
    }

    public void saveOrUpdate(FriendRequest friendRequest){
        friendRequestRepository.save(friendRequest);
    }

    public FriendRequest findBySenderAndReciever(String sender, String reciever){
        return friendRequestRepository.findBySenderAndReciever(sender, reciever);
    }

    public FriendRequest findById(int id){
        return friendRequestRepository.findById(id);
    }

    public void deleteFriendRequest(FriendRequest friendRequest){
        friendRequestRepository.delete(friendRequest);
    }
}
