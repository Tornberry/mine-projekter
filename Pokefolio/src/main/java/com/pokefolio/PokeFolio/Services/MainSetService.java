package com.pokefolio.PokeFolio.Services;

import com.pokefolio.PokeFolio.Models.MainSet;
import com.pokefolio.PokeFolio.Repositories.mainSetRepository;
import net.bytebuddy.agent.builder.AgentBuilder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MainSetService {

    private mainSetRepository mainSetRepository;

    public MainSetService(com.pokefolio.PokeFolio.Repositories.mainSetRepository mainSetRepository) {
        this.mainSetRepository = mainSetRepository;
    }

    public void saveOrUpdateList(List<MainSet> sets){
        mainSetRepository.saveAll(sets);
    }

    public Iterable<MainSet> findAll(){
        return mainSetRepository.findAll();
    }

    public void clearMainSets(){
        mainSetRepository.deleteAll();
    }
}
