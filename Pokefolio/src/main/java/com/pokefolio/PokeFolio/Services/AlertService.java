package com.pokefolio.PokeFolio.Services;

import com.pokefolio.PokeFolio.Models.website.Alert;
import org.springframework.stereotype.Service;

@Service
public class AlertService {

    private com.pokefolio.PokeFolio.Repositories.alertRepository alertRepository;

    public AlertService(com.pokefolio.PokeFolio.Repositories.alertRepository alertRepository) {
        this.alertRepository = alertRepository;
    }

    public Iterable<Alert> findAll(){
        return alertRepository.findAll();
    }

    public void saveOrUpdate(Alert alert){
        alertRepository.save(alert);
    }

    public void saveOrUpdateAll(Iterable<Alert> alerts){
        alertRepository.saveAll(alerts);
    }

    public Alert findById(int id){
        return alertRepository.findById(id);
    }

    public void deleteAlert(Alert alert){
        alertRepository.delete(alert);
    }
}
