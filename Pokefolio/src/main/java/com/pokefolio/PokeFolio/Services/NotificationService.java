package com.pokefolio.PokeFolio.Services;

import com.pokefolio.PokeFolio.Models.Notification;
import com.pokefolio.PokeFolio.Repositories.notificationRepository;
import org.springframework.stereotype.Service;

@Service
public class NotificationService {

    private notificationRepository notificationRepository;

    public NotificationService(com.pokefolio.PokeFolio.Repositories.notificationRepository notificationRepository) {
        this.notificationRepository = notificationRepository;
    }

    public void saveOrUpdate(Notification notification){
        notificationRepository.save(notification);
    }
}
