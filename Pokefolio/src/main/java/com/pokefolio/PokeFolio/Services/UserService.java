package com.pokefolio.PokeFolio.Services;

import com.pokefolio.PokeFolio.Models.user.Role;
import com.pokefolio.PokeFolio.Models.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class UserService {

    private com.pokefolio.PokeFolio.Repositories.userRepository userRepository;

    @Autowired
    public UserService(com.pokefolio.PokeFolio.Repositories.userRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void createUser(User user){
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        user.setPassword(encoder.encode(user.getPassword()));

        Role userRole = new Role("USER");
        Set<Role> roles = new HashSet<>();
        roles.add(userRole);
        user.setRoles(roles);
        user.setDateJoined(new SimpleDateFormat("yyyy.MM.dd").format(Calendar.getInstance().getTime()));

        userRepository.save(user);
   }

    public void createAdmin(User user){
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        user.setPassword(encoder.encode(user.getPassword()));

        Role userRole = new Role("ADMIN");
        Set<Role> roles = new HashSet<>();
        roles.add(userRole);
        user.setRoles(roles);
        user.setDateJoined(new SimpleDateFormat("yyyy.MM.dd").format(Calendar.getInstance().getTime()));

        userRepository.save(user);
    }

    public User findOne(String email){
        return userRepository.findByEmail(email);
    }

    public User findByUsername(String username){
        return userRepository.findByUsername(username);
    }

    public boolean isUserPresent(String email){

        User user = userRepository.findByEmail(email);
        if (user != null){
            return true;
        } else {
            return false;
        }

    }

    public Iterable<User> findAll() {
        return userRepository.findAll();
    }

    public void deleteUserByUsername(String username){
        userRepository.delete(userRepository.findByUsername(username));
    }

    public void saveOrUpdate(User user){
        userRepository.save(user);
    }

    private List<User> getUserList(){

        return new ArrayList<>(userRepository.findAll());

    }

    public void resetAllUserCollections(){

        List<User> users = getUserList();

        users.forEach(u -> u.setCollectionSetupStatus(0));

        userRepository.saveAll(users);

    }

}
