package com.pokefolio.PokeFolio.Services;

import com.pokefolio.PokeFolio.Models.Card;
import com.pokefolio.PokeFolio.Repositories.cardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CardService {

    private cardRepository cardRepository;

    @Autowired
    public CardService(com.pokefolio.PokeFolio.Repositories.cardRepository cardRepository) {
        this.cardRepository = cardRepository;
    }

    public void saveCard(Card card) {
        cardRepository.save(card);
    }

    public List<Card> getCardsBySet(String set) {
        return cardRepository.findByCardSet(set);
    }

    public List<Card> getCardsByName(String name) {
        return cardRepository.findByNameContaining(name);
    }

    public Iterable<Card> getAllCards() {
        return cardRepository.findAll();
    }

    public Card getCardById(String id) {
        return cardRepository.findById(id);
    }

    public void clearCards(){ cardRepository.deleteAll(); }

    public void saveAll(List<Card> cards){
        cardRepository.saveAll(cards);
    }

    public Card getCardByCardId(String id){
        return cardRepository.findByCardId(id);
    }
}
