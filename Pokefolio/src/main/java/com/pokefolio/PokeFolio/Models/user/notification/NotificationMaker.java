package com.pokefolio.PokeFolio.Models.user.notification;

import com.pokefolio.PokeFolio.Models.Notification;
import com.pokefolio.PokeFolio.Models.user.User;
import jdk.tools.jaotc.LoadedClass;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Date;

public class NotificationMaker {

    private static String dateFactory(){
        LocalDateTime localDateTime = LocalDateTime.now();

        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM);

        return localDateTime.format(formatter);
    }

    public static Notification makeFriendRequest(User sender, User reciever){
        return new Notification(2, dateFactory(), String.format("User with username %s wishes to be your friend", sender.getUsername()), 0, sender.getUsername());
    }

    public static Notification makeFriendRequestAccepted(User sender, User reciever){
        return new Notification(3, dateFactory(), String.format("%s accepted your friend request", reciever.getUsername()), 0, sender.getUsername());
    }

}
