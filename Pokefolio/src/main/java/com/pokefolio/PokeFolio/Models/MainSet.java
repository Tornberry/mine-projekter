package com.pokefolio.PokeFolio.Models;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class MainSet {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String name;
    @OneToMany(cascade = CascadeType.ALL)
    private List<CardSet> sets = new ArrayList<>();

    public MainSet() {
    }

    public MainSet(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CardSet> getSets() {
        return sets;
    }

    public void setSets(List<CardSet> sets) {
        this.sets = sets;
    }

    public void addSet(CardSet cardSet){
        this.sets.add(cardSet);
    }
}

