package com.pokefolio.PokeFolio.Models.user.notification.notificationrecievers;

public class AcceptFriendRequestReciever {
    private String id;

    public AcceptFriendRequestReciever() {
    }

    public AcceptFriendRequestReciever(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
