package com.pokefolio.PokeFolio.Models;

import com.pokefolio.PokeFolio.Models.user.User;

import javax.persistence.*;

@Entity
@Table(name = "notificationmessage")
public class Notification {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "notification_id")
    private int notification_id;
    private int type;
    private String time;
    private String message;
    private int readStatus;
    private String sender;

    public Notification() {
    }

    public Notification(int type, String time, String message, int readStatus, String sender) {
        this.type = type;
        this.time = time;
        this.message = message;
        this.readStatus = readStatus;
        this.sender = sender;
    }

    public int getNotification_id() {
        return notification_id;
    }

    public void setNotification_id(int notification_id) {
        this.notification_id = notification_id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getReadStatus() {
        return readStatus;
    }

    public void setReadStatus(int readStatus) {
        this.readStatus = readStatus;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }
}
