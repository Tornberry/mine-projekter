package com.pokefolio.PokeFolio.Models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class CardSet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String expandedLegal;
    private String series;
    private String releaseDate;
    private String ptcgocode;
    private String standardLegal;
    private String name;
    private String totalCards;
    private String logoUrl;
    private String symbolUrl;
    private String code;

    public CardSet() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getExpandedLegal() {
        return expandedLegal;
    }

    public void setExpandedLegal(String expandedLegal) {
        this.expandedLegal = expandedLegal;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getPtcgocode() {
        return ptcgocode;
    }

    public void setPtcgocode(String ptcgocode) {
        this.ptcgocode = ptcgocode;
    }

    public String getStandardLegal() {
        return standardLegal;
    }

    public void setStandardLegal(String standardLegal) {
        this.standardLegal = standardLegal;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTotalCards() {
        return totalCards;
    }

    public void setTotalCards(String totalCards) {
        this.totalCards = totalCards;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getSymbolUrl() {
        return symbolUrl;
    }

    public void setSymbolUrl(String symbolUrl) {
        this.symbolUrl = symbolUrl;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
