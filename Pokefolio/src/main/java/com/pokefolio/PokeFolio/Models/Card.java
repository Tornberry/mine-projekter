package com.pokefolio.PokeFolio.Models;

import com.pokefolio.PokeFolio.Models.user.collection.PersonalCard;

import javax.persistence.*;
import java.util.List;

@Entity
public class Card {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String cardId;
    private String series;
    private String imageUrlHiRes;
    private String hp;
    private String nationalPokedexNumber;
    private String imageUrl;
    private String cardSet;
    private String rarity;
    @OneToMany(cascade = CascadeType.ALL)
    private List<Weaknesses> weaknesses;
    @OneToMany(cascade = CascadeType.ALL)
    private List<Resistance> resistances;
    private String number;
    private String supertype;
    private String convertedRetreatCost;
    private String setCode;
    @ElementCollection
    private List<String> retreatCost;
    private String name;
    private String subtype;
    @ElementCollection
    private List<String> evolvesTo;
    private String artist;
    @ElementCollection
    private List<String> types;
    @OneToMany(cascade = CascadeType.ALL)
    private List<Attack> attacks;
    @ElementCollection
    @Lob
    @Column( length = 10000 )
    private List<String> text;
    private String evolvesFrom;
    private String level;
    @OneToMany(cascade = CascadeType.ALL)
    @Lob
    @Column( length = 10000 )
    private List<Ability> abilities;
    @OneToMany(mappedBy = "card")
    private List<PersonalCard> personalCards;

    public Card() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getImageUrlHiRes() {
        return imageUrlHiRes;
    }

    public void setImageUrlHiRes(String imageUrlHiRes) {
        this.imageUrlHiRes = imageUrlHiRes;
    }

    public String getHp() {
        return hp;
    }

    public void setHp(String hp) {
        this.hp = hp;
    }

    public String getNationalPokedexNumber() {
        return nationalPokedexNumber;
    }

    public void setNationalPokedexNumber(String nationalPokedexNumber) {
        this.nationalPokedexNumber = nationalPokedexNumber;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getCardSet() {
        return cardSet;
    }

    public void setCardSet(String cardSet) {
        this.cardSet = cardSet;
    }

    public String getRarity() {
        return rarity;
    }

    public void setRarity(String rarity) {
        this.rarity = rarity;
    }

    public List<Weaknesses> getWeaknesses() {
        return weaknesses;
    }

    public void setWeaknesses(List<Weaknesses> weaknesses) {
        this.weaknesses = weaknesses;
    }

    public List<Resistance> getResistances() {
        return resistances;
    }

    public void setResistances(List<Resistance> resistances) {
        this.resistances = resistances;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getSupertype() {
        return supertype;
    }

    public void setSupertype(String supertype) {
        this.supertype = supertype;
    }

    public String getConvertedRetreatCost() {
        return convertedRetreatCost;
    }

    public void setConvertedRetreatCost(String convertedRetreatCost) {
        this.convertedRetreatCost = convertedRetreatCost;
    }

    public String getSetCode() {
        return setCode;
    }

    public void setSetCode(String setCode) {
        this.setCode = setCode;
    }

    public List<String> getRetreatCost() {
        return retreatCost;
    }

    public void setRetreatCost(List<String> retreatCost) {
        this.retreatCost = retreatCost;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubtype() {
        return subtype;
    }

    public void setSubtype(String subtype) {
        this.subtype = subtype;
    }

    public List<String> getEvolvesTo() {
        return evolvesTo;
    }

    public void setEvolvesTo(List<String> evolvesTo) {
        this.evolvesTo = evolvesTo;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    public List<Attack> getAttacks() {
        return attacks;
    }

    public void setAttacks(List<Attack> attacks) {
        this.attacks = attacks;
    }

    public List<String> getText() {
        return text;
    }

    public void setText(List<String> text) {
        this.text = text;
    }

    public String getEvolvesFrom() {
        return evolvesFrom;
    }

    public void setEvolvesFrom(String evolvesFrom) {
        this.evolvesFrom = evolvesFrom;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public List<Ability> getAbilities() {
        return abilities;
    }

    public void setAbilities(List<Ability> abilities) {
        this.abilities = abilities;
    }

    public List<PersonalCard> getPersonalCards() {
        return personalCards;
    }

    public void setPersonalCards(List<PersonalCard> personalCards) {
        this.personalCards = personalCards;
    }
}
