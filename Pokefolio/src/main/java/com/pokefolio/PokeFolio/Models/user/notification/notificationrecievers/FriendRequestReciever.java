package com.pokefolio.PokeFolio.Models.user.notification.notificationrecievers;

import java.security.Principal;

public class FriendRequestReciever {

    private String reciever;

    public FriendRequestReciever() {
    }

    public FriendRequestReciever(String reciever) {
        this.reciever = reciever;
    }

    public String getReciever() {
        return reciever;
    }

    public void setReciever(String reciever) {
        this.reciever = reciever;
    }
}
