package com.pokefolio.PokeFolio.Models.user;


import com.pokefolio.PokeFolio.Models.Notification;
import com.pokefolio.PokeFolio.Models.user.collection.PersonalCard;

import javax.persistence.*;
import java.util.List;
import java.util.Set;


@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id")
    private int id;
    @Column(name = "email", unique = true)
    private String email;
    @Column(name = "username", unique = true)
    private String username;
    @Column(name = "password")
    private String password;
    @Column(name = "active")
    private int active;
    private int collectionSetupStatus = 0;
    private String profilePicUrl = "/userResourses/defaultAvatar.jpg";
    private String profileBannerUrl = "/userResourses/defaultBanner.jpg";
    private String dateJoined;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "user_notification", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "notification_id"))
    private List<Notification> notifications;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    private List<PersonalCard> collection;
    @OneToMany
    private List<User> friends;
    @OneToMany()
    List<FriendRequest> friendRequests;

    public User() {
    }

    public User(User user) {
        this.active = user.getActive();
        this.email = user.getEmail();
        this.roles = user.getRoles();
        this.username = user.getUsername();
        this.id = user.getId();
        this.password = user.getPassword();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public List<PersonalCard> getCollection() {
        return collection;
    }

    public void setCollection(List<PersonalCard> collection) {
        this.collection = collection;
    }

    public int getCollectionSetupStatus() {
        return collectionSetupStatus;
    }

    public void setCollectionSetupStatus(int collectionSetupStatus) {
        this.collectionSetupStatus = collectionSetupStatus;
    }

    public String getProfilePicUrl() {
        return profilePicUrl;
    }

    public void setProfilePicUrl(String profilePicUrl) {
        this.profilePicUrl = profilePicUrl;
    }

    public String getProfileBannerUrl() {
        return profileBannerUrl;
    }

    public void setProfileBannerUrl(String profileBannerUrl) {
        this.profileBannerUrl = profileBannerUrl;
    }

    public String getDateJoined() {
        return dateJoined;
    }

    public void setDateJoined(String dateJoined) {
        this.dateJoined = dateJoined;
    }

    public List<Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<Notification> notifications) {
        this.notifications = notifications;
    }

    public void addNotification(Notification notification){
        this.notifications.add(notification);
    }

    public List<User> getFriends() {
        return friends;
    }

    public void setFriends(List<User> friends) {
        this.friends = friends;
    }

    public void addFriend(User friend){
        this.friends.add(friend);
    }

    public void clearNotifications(){
        this.notifications.clear();
    }

    public List<FriendRequest> getFriendRequests() {
        return friendRequests;
    }

    public void setFriendRequests(List<FriendRequest> friendRequests) {
        this.friendRequests = friendRequests;
    }

    public void addFriendRequest(FriendRequest friendRequest){
        this.friendRequests.add(friendRequest);
    }

    public void removeFriendRequest(FriendRequest friendRequest){
        this.friendRequests.remove(friendRequest);
    }

    public void removeFriend(User friend){
        this.friends.remove(friend);
    }
}

