package com.pokefolio.PokeFolio.Models.user.collection;

import com.pokefolio.PokeFolio.Models.Card;
import com.pokefolio.PokeFolio.Models.user.User;

import javax.persistence.*;

@Entity
public class PersonalCard {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "pc_id")
    private int id;
    @ManyToOne
    private User user;
    @ManyToOne
    @JoinColumn(name = "card_id")
    private Card card;
    @Column(name = "amount")
    private int amount;
    private String fromSet;

    public PersonalCard() {
    }

    public PersonalCard(User user, Card card, int amount, String fromSet) {
        this.user = user;
        this.card = card;
        this.amount = amount;
        this.fromSet = fromSet;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getFromSet() {
        return fromSet;
    }

    public void setFromSet(String fromSet) {
        this.fromSet = fromSet;
    }
}
