package com.pokefolio.PokeFolio.Models;

import javax.persistence.*;
import java.util.List;

@Entity
public class Attack {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Lob
    @Column( length = 100 )
    private String text;
    private String convertedEnergyCost;
    private String name;
    private String damage;
    @ElementCollection
    private List<String> cost;

    public Attack(String text, String convertedEnergyCost, String name, String damage, List<String> cost) {
        this.text = text;
        this.convertedEnergyCost = convertedEnergyCost;
        this.name = name;
        this.damage = damage;
        this.cost = cost;
    }

    public Attack() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getConvertedEnergyCost() {
        return convertedEnergyCost;
    }

    public void setConvertedEnergyCost(String convertedEnergyCost) {
        this.convertedEnergyCost = convertedEnergyCost;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDamage() {
        return damage;
    }

    public void setDamage(String damage) {
        this.damage = damage;
    }

    public List<String> getCost() {
        return cost;
    }

    public void setCost(List<String> cost) {
        this.cost = cost;
    }
}
