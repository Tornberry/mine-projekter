package com.pokefolio.PokeFolio.Models.website;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Alert {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String type;
    private String title;
    private String message;
    private boolean active;
    private String timeActivated;
    private String author;

    public Alert() {
    }

    public Alert(String type, String title, String message, boolean active, String timeActivated, String author) {
        this.type = type;
        this.title = title;
        this.message = message;
        this.active = active;
        this.timeActivated = timeActivated;
        this.author = author;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getTimeActivated() {
        return timeActivated;
    }

    public void setTimeActivated(String timeActivated) {
        this.timeActivated = timeActivated;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
