package com.pokefolio.PokeFolio.Models.user.notification.notificationrecievers;

public class DeclineFriendRequestReciever {
    private String id;

    public DeclineFriendRequestReciever() {
    }

    public DeclineFriendRequestReciever(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
