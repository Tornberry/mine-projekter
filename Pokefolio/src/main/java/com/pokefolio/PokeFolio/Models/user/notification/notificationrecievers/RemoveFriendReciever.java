package com.pokefolio.PokeFolio.Models.user.notification.notificationrecievers;

public class RemoveFriendReciever {
    private String username;

    public RemoveFriendReciever() {
    }

    public RemoveFriendReciever(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
