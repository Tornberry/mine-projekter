package com.pokefolio.PokeFolio.JSON;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import com.pokefolio.PokeFolio.Models.*;
import org.springframework.stereotype.Service;

import java.io.*;
import java.lang.reflect.Type;
import java.util.*;

@Service
public class CustomAPI {

    public List<CardSet> makeSetListFromFile(String filePath) throws IOException {

        File setFile = new File(filePath);
        BufferedReader bufferedReader = new BufferedReader(new FileReader(setFile));
        StringBuilder stringBuilder = new StringBuilder();

        String line = bufferedReader.readLine();

        while (line != null){
            stringBuilder.append(line);
            stringBuilder.append(System.lineSeparator());
            line = bufferedReader.readLine();
        }

        String everything = stringBuilder.toString();

        Gson gson = new Gson();

        Type collectionType = new TypeToken<Collection<CardSet>>(){}.getType();
        Collection<CardSet> cardSets = gson.fromJson(everything, collectionType);

        List<CardSet> List = new ArrayList<>();

        for (CardSet cardSet : cardSets){
            List.add(cardSet);
        }

        bufferedReader.close();

        return List;
    }

    public List<Card> makeCardListFromFile(String filePath) throws IOException {

        File cardFile = new File(filePath);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(cardFile), "UTF-8"));
        StringBuilder stringBuilder = new StringBuilder();

        String line = bufferedReader.readLine();

        while (line != null){
            stringBuilder.append(line);
            stringBuilder.append(System.lineSeparator());
            line = bufferedReader.readLine();
        }

        String everything = stringBuilder.toString();

        //--------------------------------------------------------------------------------------------------------------------------------------------------

        GsonBuilder gsonBuilder = new GsonBuilder();

        JsonDeserializer<Card> cardDeserializer = new JsonDeserializer<Card>() {
            @Override
            public Card deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

                JsonObject jsonObject = json.getAsJsonObject();
                Gson gson = new Gson();

                switch (jsonObject.get("supertype").getAsString()){
                    case "Pokémon":
                        JsonElement pokemon_id = jsonObject.get("id");
                        JsonElement pokemon_name = jsonObject.get("name");
                        JsonElement pokemon_imageUrl = jsonObject.get("imageUrl");
                        JsonElement pokemon_subtype = jsonObject.get("subtype");
                        JsonElement pokemon_supertype = jsonObject.get("supertype");
                        JsonElement pokemon_level = jsonObject.get("level");
                        JsonElement pokemon_evolvesFrom = jsonObject.get("evolvesFrom");
                        JsonElement pokemon_abilities = jsonObject.get("ability");
                        JsonElement pokemon_hp = jsonObject.get("hp");
                        JsonElement pokemon_retreatCost = jsonObject.get("retreatCost");
                        JsonElement pokemon_convertedRetreatCost = jsonObject.get("convertedRetreatCost");
                        JsonElement pokemon_number = jsonObject.get("number");
                        JsonElement pokemon_artist = jsonObject.get("artist");
                        JsonElement pokemon_rarity = jsonObject.get("rarity");
                        JsonElement pokemon_series = jsonObject.get("series");
                        JsonElement pokemon_set = jsonObject.get("set");
                        JsonElement pokemon_setCode = jsonObject.get("setCode");
                        JsonElement pokemon_types = jsonObject.get("types");
                        JsonElement pokemon_attacks = jsonObject.get("attacks");
                        JsonElement pokemon_weaknesses = jsonObject.get("weaknesses");
                        JsonElement pokemon_resistances = jsonObject.get("resistances");
                        JsonElement pokemon_imageUrlHiRes = jsonObject.get("imageUrlHiRes");
                        JsonElement pokemon_nationalPokedexNumber = jsonObject.get("nationalPokedexNumber");
                        JsonElement pokemon_evolvesTo = jsonObject.get("evolvesTo");

                        Card pokemon_card = new Card();

                        try {
                            pokemon_card.setCardId(pokemon_id.getAsString());
                        } catch (Exception e){

                        }

                        try {
                            pokemon_card.setName(pokemon_name.getAsString());
                        } catch (Exception e){

                        }

                        try {
                            pokemon_card.setImageUrl(pokemon_imageUrl.getAsString());
                        } catch (Exception e){

                        }

                        try {
                            pokemon_card.setSubtype(pokemon_subtype.getAsString());
                        } catch (Exception e){

                        }

                        try {
                            pokemon_card.setSupertype(pokemon_supertype.getAsString());
                        } catch (Exception e){

                        }

                        try {
                            pokemon_card.setLevel(pokemon_level.getAsString());
                        } catch (Exception e){

                        }

                        try {
                            pokemon_card.setEvolvesFrom(pokemon_evolvesFrom.getAsString());
                        } catch (Exception e){

                        }

                        try {
                            Ability ability = gson.fromJson(pokemon_abilities, new TypeToken<Ability>(){}.getType());
                            List<Ability> abilities = new ArrayList<>();
                            abilities.add(ability);
                            pokemon_card.setAbilities(abilities);
                        } catch (Exception e){

                        }

                        try {
                            pokemon_card.setHp(pokemon_hp.getAsString());
                        } catch (Exception e){

                        }

                        try {
                            List<String> retreatCostList = gson.fromJson(pokemon_retreatCost, new TypeToken<List<String>>(){}.getType());
                            pokemon_card.setRetreatCost(retreatCostList);
                        } catch (Exception e){

                        }

                        try {
                            pokemon_card.setConvertedRetreatCost(pokemon_convertedRetreatCost.getAsString());
                        } catch (Exception e){

                        }

                        try {
                            pokemon_card.setNumber(pokemon_number.getAsString());
                        } catch (Exception e){

                        }

                        try {
                            pokemon_card.setArtist(pokemon_artist.getAsString());
                        } catch (Exception e){

                        }

                        try {
                            pokemon_card.setRarity(pokemon_rarity.getAsString());
                        } catch (Exception e){

                        }

                        try {
                            pokemon_card.setSeries(pokemon_series.getAsString());
                        } catch (Exception e){

                        }

                        try {
                            pokemon_card.setCardSet(pokemon_set.getAsString());
                        } catch (Exception e){

                        }

                        try {
                            pokemon_card.setSetCode(pokemon_setCode.getAsString());
                        } catch (Exception e){

                        }

                        try {
                            List<String> typesList = gson.fromJson(pokemon_types, new TypeToken<List<String>>(){}.getType());
                            pokemon_card.setTypes(typesList);
                        } catch (Exception e){

                        }

                        try {
                            List<Attack> attackList = gson.fromJson(pokemon_attacks, new TypeToken<List<Attack>>(){}.getType());
                            pokemon_card.setAttacks(attackList);
                        } catch (Exception e){

                        }

                        try {
                            List<Weaknesses> weaknessesList = gson.fromJson(pokemon_weaknesses, new TypeToken<List<Weaknesses>>(){}.getType());
                            pokemon_card.setWeaknesses(weaknessesList);
                        } catch (Exception e){

                        }

                        try {
                            List<Resistance> resistancesList = gson.fromJson(pokemon_resistances, new TypeToken<List<Resistance>>(){}.getType());
                            pokemon_card.setResistances(resistancesList);
                        } catch (Exception e){

                        }

                        try {
                            pokemon_card.setImageUrlHiRes(pokemon_imageUrlHiRes.getAsString());
                        } catch (Exception e){

                        }

                        try {
                            pokemon_card.setNationalPokedexNumber(pokemon_nationalPokedexNumber.getAsString());
                        } catch (Exception e){

                        }

                        try{
                            List<String> evolvesToList = gson.fromJson(pokemon_evolvesTo, new TypeToken<List<String>>(){}.getType());
                            pokemon_card.setEvolvesTo(evolvesToList);
                        } catch (Exception e){

                        }

                        //System.out.println(pokemon_card.getName());

                        return pokemon_card;
                    case "Trainer":
                        JsonElement trainer_id = jsonObject.get("id");
                        JsonElement trainer_name = jsonObject.get("name");
                        JsonElement trainer_imageUrl = jsonObject.get("imageUrl");
                        JsonElement trainer_subtype = jsonObject.get("subtype");
                        JsonElement trainer_number = jsonObject.get("number");
                        JsonElement trainer_artist = jsonObject.get("artist");
                        JsonElement trainer_rarity = jsonObject.get("rarity");
                        JsonElement trainer_series = jsonObject.get("series");
                        JsonElement trainer_set = jsonObject.get("set");
                        JsonElement trainer_setCode = jsonObject.get("setCode");
                        JsonElement trainer_text = jsonObject.get("text");
                        JsonElement trainer_imageUrlHiRes = jsonObject.get("imageUrlHiRes");
                        JsonElement trainer_superType = jsonObject.get("supertype");

                        Card trainer_card = new Card();


                        try{
                            trainer_card.setCardId(trainer_id.getAsString());
                        } catch (Exception e){

                        }

                        try{
                            trainer_card.setName(trainer_name.getAsString());
                        } catch (Exception e){

                        }

                        try{
                            trainer_card.setImageUrl(trainer_imageUrl.getAsString());
                        } catch (Exception e){

                        }

                        try{
                            trainer_card.setSubtype(trainer_subtype.getAsString());
                        } catch (Exception e){

                        }

                        try{
                            trainer_card.setNumber(trainer_number.getAsString());
                        } catch (Exception e){

                        }

                        try{
                            trainer_card.setArtist(trainer_artist.getAsString());
                        } catch (Exception e){

                        }

                        try{
                            trainer_card.setRarity(trainer_rarity.getAsString());
                        } catch (Exception e){

                        }

                        try{
                            trainer_card.setSeries(trainer_series.getAsString());
                        } catch (Exception e){

                        }

                        try{
                            trainer_card.setCardSet(trainer_set.getAsString());
                        } catch (Exception e){

                        }

                        try{
                            trainer_card.setSetCode(trainer_setCode.getAsString());
                        } catch (Exception e){

                        }

                        try {
                            List<String> textList = gson.fromJson(trainer_text, new TypeToken<List<String>>(){}.getType());
                            trainer_card.setText(textList);
                        } catch (Exception e){

                        }

                        try {
                            trainer_card.setImageUrlHiRes(trainer_imageUrlHiRes.getAsString());
                        } catch (Exception e){

                        }

                        try {
                            trainer_card.setSupertype(trainer_superType.getAsString());
                        } catch (Exception e){

                        }

                        //System.out.println(trainer_card.getName());

                        return trainer_card;
                    case "Energy":
                        JsonElement energy_id = jsonObject.get("id");
                        JsonElement energy_name = jsonObject.get("name");
                        JsonElement energy_imageUrl = jsonObject.get("imageUrl");
                        JsonElement energy_subtype = jsonObject.get("subtype");
                        JsonElement energy_number = jsonObject.get("number");
                        JsonElement energy_artist = jsonObject.get("artist");
                        JsonElement energy_rarity = jsonObject.get("rarity");
                        JsonElement energy_series = jsonObject.get("series");
                        JsonElement energy_set = jsonObject.get("set");
                        JsonElement energy_setCode = jsonObject.get("setCode");
                        JsonElement energy_text = jsonObject.get("text");
                        JsonElement energy_imageUrlHiRes = jsonObject.get("imageUrlHiRes");
                        JsonElement energy_superType = jsonObject.get("supertype");

                        Card energy_card = new Card();

                        energy_card.setCardId(energy_id.getAsString());
                        energy_card.setName(energy_name.getAsString());
                        energy_card.setImageUrl(energy_imageUrl.getAsString());
                        energy_card.setSubtype(energy_subtype.getAsString());
                        energy_card.setNumber(energy_number.getAsString());
                        energy_card.setArtist(energy_artist.getAsString());
                        energy_card.setRarity(energy_rarity.getAsString());
                        energy_card.setSeries(energy_series.getAsString());
                        energy_card.setCardSet(energy_set.getAsString());
                        energy_card.setSetCode(energy_setCode.getAsString());
                        energy_card.setSupertype(energy_superType.getAsString());

                        try {
                            List<String> textList = gson.fromJson(energy_text, new TypeToken<List<String>>(){}.getType());
                            energy_card.setText(textList);
                        } catch (Exception e){

                        }

                        energy_card.setImageUrlHiRes(energy_imageUrlHiRes.getAsString());

                        //System.out.println(energy_card.getName());

                        return energy_card;
                        default:
                            System.out.println("Card supertype not recognized. Returning null");
                            return null;


                }

            }
        };

        gsonBuilder.registerTypeAdapter(Card.class, cardDeserializer);

        Gson gson = gsonBuilder.create();

        Type collectionType = new TypeToken<List<Card>>(){}.getType();
        List<Card> cardList = gson.fromJson(everything, collectionType);

        bufferedReader.close();

        return cardList;
    }
}
