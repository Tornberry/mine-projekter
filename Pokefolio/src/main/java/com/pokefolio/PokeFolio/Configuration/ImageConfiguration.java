package com.pokefolio.PokeFolio.Configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class ImageConfiguration implements WebMvcConfigurer {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/userResourses/**").addResourceLocations("file:C:/Users/Administrator/Pokefolio/Users/");
        registry.addResourceHandler("/graphicResources/**").addResourceLocations("file:C:/Users/Administrator/Pokefolio/Graphics/");
    }
}
