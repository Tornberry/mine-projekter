package com.pokefolio.PokeFolio.Controllers.test;

import com.pokefolio.PokeFolio.Services.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.security.Principal;

@Controller
@RequestMapping("/test")
public class TestController {

    private UserService userService;

    public TestController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/testpc")
    @ResponseBody
    public String friendRequestMesasge(Principal principal){

        //Notification notification = NotificationMaker.makeFriendRequest(userService.findByUsername(principal.getName()));

        return "gottem";
    }

}
