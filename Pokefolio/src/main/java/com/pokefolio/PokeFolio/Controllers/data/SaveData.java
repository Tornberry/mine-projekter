package com.pokefolio.PokeFolio.Controllers.data;

import com.pokefolio.PokeFolio.JSON.CustomAPI;
import com.pokefolio.PokeFolio.Models.Card;
import com.pokefolio.PokeFolio.Models.CardSet;
import com.pokefolio.PokeFolio.Models.MainSet;
import com.pokefolio.PokeFolio.Services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Controller
@PreAuthorize("hasAnyRole('ADMIN')")
@RequestMapping("/data")
public class SaveData {

    private SetService setService;
    private CustomAPI customAPI;
    private CardService cardService;
    private PersonalCardService personalCardService;
    private UserService userService;
    private MainSetService mainSetService;

    @Autowired
    public SaveData(SetService setService, CustomAPI customAPI, CardService cardService, PersonalCardService personalCardService, UserService userService, MainSetService mainSetService) {
        this.setService = setService;
        this.customAPI = customAPI;
        this.cardService = cardService;
        this.personalCardService = personalCardService;
        this.userService = userService;
        this.mainSetService = mainSetService;
    }

    @GetMapping("/updateData")
    @ResponseBody
    public String updateDataServer() throws IOException, InterruptedException {

        mainSetService.clearMainSets();
        System.out.println("Sets cleared!");
        Thread.sleep(5000);
        personalCardService.clearPC();
        System.out.println("Personal cards cleared!");
        Thread.sleep(5000);
        cardService.clearCards();
        System.out.println("Cards cleared!");
        Thread.sleep(5000);

        List<CardSet> sets = customAPI.makeSetListFromFile("C:\\Users\\Administrator\\Pokefolio\\Data\\sets\\sets.json");

        List<MainSet> mainSets = new ArrayList<>();

        boolean foundMainSet = false;
        for (CardSet set : sets){
            for (MainSet mainSet : mainSets){
                if (mainSet.getName().equals(set.getSeries())){
                    mainSet.addSet(set);
                    foundMainSet = true;
                    break;
                }
            }
            if (!foundMainSet){
                MainSet newMainSet = new MainSet(set.getSeries());
                newMainSet.addSet(set);
                mainSets.add(newMainSet);
            }
            foundMainSet = false;
        }

        mainSetService.saveOrUpdateList(mainSets);

        //setService.saveAll(sets);

        System.out.println("Sets updated!");
        Thread.sleep(5000);

        File dirLocal = new File("C:\\Users\\Administrator\\Pokefolio\\Data\\cards");
        List<File> files = new ArrayList<>(Arrays.asList(Objects.requireNonNull(dirLocal.listFiles())));

        for (File file : files){
            List<Card> cardList = customAPI.makeCardListFromFile(file.getPath());
            cardService.saveAll(cardList);
        }

        System.out.println("Cards updated!");

        Thread.sleep(5000);

        userService.resetAllUserCollections();

        System.out.println("Every collection reset!");

        return "done";
    }

    @GetMapping("/updateDataLocal")
    @ResponseBody
    public String updateDataLocal() throws IOException, InterruptedException {

        mainSetService.clearMainSets();
        System.out.println("Sets cleared!");
        Thread.sleep(5000);
        personalCardService.clearPC();
        System.out.println("Personal cards cleared!");
        Thread.sleep(5000);
        cardService.clearCards();
        System.out.println("Cards cleared!");
        Thread.sleep(5000);

        List<CardSet> sets = customAPI.makeSetListFromFile("C:\\Users\\Simon\\Desktop\\data\\sets\\sets.json");

        List<MainSet> mainSets = new ArrayList<>();

        boolean foundMainSet = false;
        for (CardSet set : sets){
            for (MainSet mainSet : mainSets){
                if (mainSet.getName().equals(set.getSeries())){
                    mainSet.addSet(set);
                    foundMainSet = true;
                    break;
                }
            }
            if (!foundMainSet){
                MainSet newMainSet = new MainSet(set.getSeries());
                newMainSet.addSet(set);
                mainSets.add(newMainSet);
            }
            foundMainSet = false;
        }

        mainSetService.saveOrUpdateList(mainSets);

        //setService.saveAll(sets);

        System.out.println("Sets updated!");
        Thread.sleep(5000);

        File dirLocal = new File("C:\\Users\\Simon\\Desktop\\data\\cards");
        List<File> files = new ArrayList<>(Arrays.asList(Objects.requireNonNull(dirLocal.listFiles())));

        for (File file : files){
            List<Card> cardList = customAPI.makeCardListFromFile(file.getPath());
            cardService.saveAll(cardList);
        }

        System.out.println("Cards updated!");

        Thread.sleep(5000);

        userService.resetAllUserCollections();

        System.out.println("Every collection reset!");

        return "done";
    }
}
