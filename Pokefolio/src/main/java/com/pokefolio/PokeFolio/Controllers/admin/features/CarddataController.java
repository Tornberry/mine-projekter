package com.pokefolio.PokeFolio.Controllers.admin.features;

import com.pokefolio.PokeFolio.Services.SetService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@PreAuthorize("hasAnyRole('ADMIN')")
@RequestMapping("/admin")
public class CarddataController {

    private SetService setService;

    public CarddataController(SetService setService) {
        this.setService = setService;
    }

    @GetMapping("/updatecarddata")
    public String getCardDataPage(Model model){
        model.addAttribute("sets", setService.findAll());
        return "admin/updatedata";
    }

}
