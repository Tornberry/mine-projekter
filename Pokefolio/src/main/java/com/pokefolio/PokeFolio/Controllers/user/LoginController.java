package com.pokefolio.PokeFolio.Controllers.user;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@SessionAttributes("")
public class LoginController {

    @GetMapping("/login")
    public String showLogin(Model model){

        return "user/login";
    }

}
