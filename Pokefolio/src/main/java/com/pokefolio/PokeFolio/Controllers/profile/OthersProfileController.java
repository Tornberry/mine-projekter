package com.pokefolio.PokeFolio.Controllers.profile;

import com.pokefolio.PokeFolio.Controllers.profile.support.Statistics.StatisticGenerator;
import com.pokefolio.PokeFolio.Models.user.FriendRequest;
import com.pokefolio.PokeFolio.Models.user.User;
import com.pokefolio.PokeFolio.Models.user.notification.notificationrecievers.FriendRequestReciever;
import com.pokefolio.PokeFolio.Services.FriendRequestService;
import com.pokefolio.PokeFolio.Services.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.security.Principal;

@Controller
public class OthersProfileController {

    private UserService userService;
    private StatisticGenerator statisticGenerator;
    private FriendRequestService friendRequestService;

    public OthersProfileController(UserService userService, StatisticGenerator statisticGenerator) {
        this.userService = userService;
        this.statisticGenerator = statisticGenerator;
    }

    @GetMapping("/profile/{username}")
    public String othersProfile(Model model, @PathVariable String username, Principal principal){

        User otheruser = userService.findByUsername(username);
        int state = 0;
        for (FriendRequest friendRequest : otheruser.getFriendRequests()){
            if (friendRequest.getSender().equals(principal.getName())){
                if (friendRequest.getState() == 0){
                    state = 1;
                }
            }
        }

        boolean friends = false;
        for(User friend : otheruser.getFriends()){
            if (friend.getUsername().equals(principal.getName())){
                friends = true;
            }
        }

        model.addAttribute("isMe", principal.getName().equals(username));
        model.addAttribute("friendsTrueFalse", friends);
        model.addAttribute("friendStatus", state);
        model.addAttribute("userProfile", userService.findByUsername(username));

        return "profile/othersprofile/othersprofile";
    }

    @GetMapping("/profile/{username}/setStatistics")
    public String othersProfileSetStats(Model model, @PathVariable String username, Principal principal){

        User otheruser = userService.findByUsername(username);
        int state = 0;
        for (FriendRequest friendRequest : otheruser.getFriendRequests()){
            if (friendRequest.getSender().equals(principal.getName())){
                if (friendRequest.getState() == 0){
                    state = 1;
                }
            }
        }

        boolean friends = false;
        for(User friend : otheruser.getFriends()){
            if (friend.getUsername().equals(principal.getName())){
                friends = true;
            }
        }

        model.addAttribute("isMe", principal.getName().equals(username));
        model.addAttribute("friendsTrueFalse", friends);
        model.addAttribute("friendStatus", state);
        model.addAttribute("setStats", statisticGenerator.getSetStatistics(userService.findByUsername(username)));
        model.addAttribute("userProfile", userService.findByUsername(username));

        return "profile/othersprofile/otherssetstatistics";
    }

    @GetMapping("/profile/{username}/friends")
    public String othersProfileFriends(Model model, @PathVariable String username, Principal principal){

        User otheruser = userService.findByUsername(username);
        int state = 0;
        for (FriendRequest friendRequest : otheruser.getFriendRequests()){
            if (friendRequest.getSender().equals(principal.getName())){
                if (friendRequest.getState() == 0){
                    state = 1;
                }
            }
        }

        boolean friends = false;
        for(User friend : otheruser.getFriends()){
            if (friend.getUsername().equals(principal.getName())){
                friends = true;
            }
        }

        model.addAttribute("isMe", principal.getName().equals(username));
        model.addAttribute("friendsTrueFalse", friends);
        model.addAttribute("friendStatus", state);
        model.addAttribute("userProfile", userService.findByUsername(username));

        return "profile/othersprofile/othersfriends";
    }

}
