package com.pokefolio.PokeFolio.Controllers.profile.support.Statistics;

public class SetStatistic {

    private String set;
    private String totalCards;
    private int owned;
    private String ownPercent;

    public SetStatistic(String name) {
        this.set = name;
    }

    public String getSet() {
        return set;
    }

    public void setSet(String set) {
        this.set = set;
    }

    public String getOwnPercent() {
        return ownPercent;
    }

    public void setOwnPercent(String ownPercent) {
        this.ownPercent = ownPercent;
    }

    public String getTotalCards() {
        return totalCards;
    }

    public void setTotalCards(String totalCards) {
        this.totalCards = totalCards;
    }

    public int getOwned() {
        return owned;
    }

    public void setOwned(int owned) {
        this.owned = owned;
    }
}
