package com.pokefolio.PokeFolio.Controllers.cards;

import com.pokefolio.PokeFolio.Controllers.cards.search.postRecievers.AmountReciever;
import com.pokefolio.PokeFolio.Models.CardSet;
import com.pokefolio.PokeFolio.Models.user.collection.PersonalCard;
import com.pokefolio.PokeFolio.Services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
public class CardDatabaseController {

    private SetService setService;
    private CardService cardService;
    private UserService userService;
    private PersonalCardService personalCardService;
    private MainSetService mainSetService;

    @Autowired
    public CardDatabaseController(SetService setService, CardService cardService, UserService userService, PersonalCardService personalCardService, MainSetService mainSetService) {
        this.setService = setService;
        this.cardService = cardService;
        this.userService = userService;
        this.personalCardService = personalCardService;
        this.mainSetService = mainSetService;
    }

    private List<CardSet> getCardSerieses(){

        List<CardSet> cardSetList = new ArrayList<>();

        List<String> serieses = new ArrayList<>();

        for (CardSet set : setService.findAll()){
          if (!serieses.contains(set.getSeries())){
              serieses.add(set.getSeries());
              cardSetList.add(set);

          }
        }

        return cardSetList;
    }

    @GetMapping("/carddatabase")
    public String showCardDatabase(Model model, @RequestParam(value = "set", defaultValue = "Ultra Prism") String set, Principal principal){

        model.addAttribute("set", setService.findByName(set));
        model.addAttribute("cards", personalCardService.getAllPersonalCardsByUserAndFromSet(userService.findByUsername(principal.getName()), set));
        model.addAttribute("mainSets", mainSetService.findAll());
        return "carddatabase/carddatabase";
    }

    @GetMapping("/carddatabase/single")
    public String showSingleCard(Model model, @RequestParam("id") String id, Principal principal){

        model.addAttribute("card", cardService.getCardByCardId(id));

        return "carddatabase/single";
    }

    @GetMapping("/carddatabase/editAmount")
    @ResponseBody
    public String editAmount(@RequestParam("pc") int pc, @RequestParam("amount") int amount, Principal principal){

        PersonalCard personalCard = personalCardService.findById(pc);

        if (principal.getName().equals(personalCard.getUser().getUsername())){
            personalCard.setAmount(amount);
            personalCardService.saveOrUpdate(personalCard);
            return "done";
        } else {
            return "done";
        }

    }

    @GetMapping("/carddatabase/{username}")
    public String userCardDatabase(Model model, @RequestParam(value = "set", defaultValue = "Ultra Prism") String set, @PathVariable String username){

        model.addAttribute("set", setService.findByName(set));
//        model.addAttribute("allSerieses", setService.findAll());
//        model.addAttribute("serieses", getCardSerieses());
        model.addAttribute("mainSets", mainSetService.findAll());
        model.addAttribute("cards", personalCardService.getAllPersonalCardsByUserAndFromSet(userService.findByUsername(username), set));
        model.addAttribute("user", userService.findByUsername(username));

        return "carddatabase/userCarddatabase";
    }

    @GetMapping("/carddatabase/setAmountOne")
    @ResponseBody
    public String setAmountOne(@RequestParam("set") String set, Principal principal){

        Iterable<PersonalCard> cards = personalCardService.getAllPersonalCardsByUserAndFromSet(userService.findByUsername(principal.getName()), set);

        for (PersonalCard personalCard : cards){
            personalCard.setAmount(1);
            personalCardService.saveOrUpdate(personalCard);
        }

        return "done";
    }

    @GetMapping("/carddatabase/setAmountZero")
    @ResponseBody
    public String setAmountZero(@RequestParam("set") String set, Principal principal){

        Iterable<PersonalCard> cards = personalCardService.getAllPersonalCardsByUserAndFromSet(userService.findByUsername(principal.getName()), set);

        for (PersonalCard personalCard : cards){
            personalCard.setAmount(0);
            personalCardService.saveOrUpdate(personalCard);
        }

        return "done";
    }

    @PostMapping("/carddatabase/addOrSubtract")
    @ResponseBody()
    public String addOne(@Valid AmountReciever amountReciever, BindingResult bindingResult){
        if (amountReciever.getAmountType() == 1){
            int id = Integer.parseInt(amountReciever.getPCID());
            PersonalCard pc = personalCardService.findById(id);
            pc.setAmount(pc.getAmount() + 1);
            personalCardService.saveOrUpdate(pc);
        } else if(amountReciever.getAmountType() == 2){
            int id = Integer.parseInt(amountReciever.getPCID());
            PersonalCard pc = personalCardService.findById(id);
            if (!(pc.getAmount() <= 0)){
                pc.setAmount(pc.getAmount() - 1);
            }
            personalCardService.saveOrUpdate(pc);
        }
        return "done";
    }

}
