package com.pokefolio.PokeFolio.Controllers.profile.support.FileUpload;

import com.pokefolio.PokeFolio.Models.user.User;
import com.pokefolio.PokeFolio.Services.UserService;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

@Service
public class FileUploadService {

    private UserService userService;

    public FileUploadService(UserService userService) {
        this.userService = userService;
    }

    public void saveBanner(User user, MultipartFile file) throws IOException {

        String username = user.getUsername();

        File banner = new File("C:\\Users\\Administrator\\Pokefolio\\Users\\" + username + "\\" + "banner" + "." + FilenameUtils.getExtension(file.getOriginalFilename()));
        banner.getParentFile().mkdirs();
        banner.createNewFile();

        FileOutputStream fileOutputStream = new FileOutputStream(banner);
        fileOutputStream.write(file.getBytes());
        fileOutputStream.close();

        user.setProfileBannerUrl("/userResourses/" + username + "/" + "banner" + "." + FilenameUtils.getExtension(file.getOriginalFilename()));
        userService.saveOrUpdate(user);
    }

    public void saveAvatar(User user, MultipartFile file) throws IOException {

        String username = user.getUsername();

        File banner = new File("C:\\Users\\Administrator\\Pokefolio\\Users\\" + username + "\\" + "avatar" + "." + FilenameUtils.getExtension(file.getOriginalFilename()));
        banner.getParentFile().mkdirs();
        banner.createNewFile();

        FileOutputStream fileOutputStream = new FileOutputStream(banner);
        fileOutputStream.write(file.getBytes());
        fileOutputStream.close();

        user.setProfilePicUrl("/userResourses/" + username + "/" + "avatar" + "." + FilenameUtils.getExtension(file.getOriginalFilename()));
        userService.saveOrUpdate(user);
    }

}
