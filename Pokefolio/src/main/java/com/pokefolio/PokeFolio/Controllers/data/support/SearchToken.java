package com.pokefolio.PokeFolio.Controllers.data.support;


public class SearchToken {

    private String series;
    private int hpStart;
    private int hpEnd;
    private String rarity;
    private String name;
    private String artist;
    private String type;

    public SearchToken() {
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public int getHpStart() {
        return hpStart;
    }

    public void setHpStart(int hpStart) {
        this.hpStart = hpStart;
    }

    public int getHpEnd() {
        return hpEnd;
    }

    public void setHpEnd(int hpEnd) {
        this.hpEnd = hpEnd;
    }

    public String getRarity() {
        return rarity;
    }

    public void setRarity(String rarity) {
        this.rarity = rarity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
