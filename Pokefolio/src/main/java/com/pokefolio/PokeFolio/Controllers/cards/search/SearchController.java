package com.pokefolio.PokeFolio.Controllers.cards.search;

import com.pokefolio.PokeFolio.Controllers.data.support.SearchToken;
import com.pokefolio.PokeFolio.Models.Card;
import com.pokefolio.PokeFolio.Models.CardSet;
import com.pokefolio.PokeFolio.Services.CardService;
import com.pokefolio.PokeFolio.Services.SetService;
import com.pokefolio.PokeFolio.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
public class SearchController {

    private CardService cardService;
    private SetService setService;
    private UserService userService;

    @Autowired
    public SearchController(CardService cardService, SetService setService, UserService userService) {
        this.cardService = cardService;
        this.setService = setService;
        this.userService = userService;
    }


    public List<Card> searchAlgorithm(SearchToken searchToken){

        Iterable<Card> allCards = cardService.getAllCards();

        List<Card> cardList = new ArrayList<>();



        for (Card card : allCards){

            boolean series_ok = false;
            boolean hp_ok = false;
            boolean rarity_ok = false;
            boolean name_ok = false;
            boolean artist_ok = false;
            boolean type_ok = false;

            try {
                if (searchToken.getSeries() != ""){
                    if (card.getSeries().toLowerCase().contains(searchToken.getSeries().toLowerCase())){
                        series_ok = true;
                    }
                } else {
                    series_ok = true;
                }
            } catch (Exception e){

            }

            try {
                if (searchToken.getHpEnd() != 0){
                    if (Integer.parseInt(card.getHp()) >= searchToken.getHpStart() && Integer.parseInt(card.getHp()) <= searchToken.getHpEnd()){
                        hp_ok= true;
                    }
                } else {
                    hp_ok = true;
                }
            } catch (Exception e){

            }

            try {
                if (searchToken.getRarity() != ""){
                    if (card.getRarity().toLowerCase().contains(searchToken.getRarity().toLowerCase())){
                       rarity_ok = true;
                    }
                } else {
                    rarity_ok = true;
                }
            } catch (Exception e){

            }

            try {
                if (searchToken.getName() != ""){
                    if (card.getName().toLowerCase().contains(searchToken.getName().toLowerCase())){
                        name_ok = true;
                    }
                } else {
                    name_ok = true;
                }
            } catch (Exception e){

            }

            try {
                if (searchToken.getArtist() != ""){
                    if (card.getArtist().toLowerCase().contains(searchToken.getArtist())){
                        artist_ok = true;
                    }
                } else {
                    artist_ok = true;
                }
            } catch (Exception e){

            }

            try {
                if (searchToken.getType() != ""){
                    if (card.getTypes().contains(searchToken.getType())){
                       type_ok = true;
                    }
                } else {
                    type_ok = true;
                }
            } catch (Exception e){

            }

            if (series_ok && hp_ok && rarity_ok && name_ok && artist_ok && type_ok){
               cardList.add(card);
            }

        }

        return cardList;

    }

    @GetMapping("/carddatabase/search")
    public String makeSearch(Model model, Principal principal){

        List<CardSet> cardSetList = new ArrayList<>();
        List<String> setNames = new ArrayList<>();

        for (CardSet cardSet : setService.findAll()){
            if (!setNames.contains(cardSet.getSeries())){
                setNames.add(cardSet.getSeries());
                cardSetList.add(cardSet);
            }
        }

        model.addAttribute("sets", cardSetList);
        model.addAttribute("searchToken", new SearchToken());

        return "carddatabase/search/search";
    }

    @PostMapping("/carddatabase/search")
    public String getSearch(@Valid SearchToken searchToken, BindingResult bindingResult, Model model, Principal principal){

        System.out.println(searchToken.getSeries());

    if (bindingResult.hasErrors()){
        System.out.println(bindingResult.getAllErrors());
    }

        List<Card> cardList = searchAlgorithm(searchToken);

        model.addAttribute("cards", cardList);
        model.addAttribute("cardsFound", cardList.size());


        return "carddatabase/search/searchResult";
    }
}
