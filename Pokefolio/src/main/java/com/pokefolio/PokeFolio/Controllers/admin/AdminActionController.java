package com.pokefolio.PokeFolio.Controllers.admin;

import com.pokefolio.PokeFolio.Models.user.Role;
import com.pokefolio.PokeFolio.Models.user.User;
import com.pokefolio.PokeFolio.Models.website.Alert;
import com.pokefolio.PokeFolio.Services.AlertService;
import com.pokefolio.PokeFolio.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import javax.validation.Valid;
import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

@Controller
@PreAuthorize("hasAnyRole('ADMIN')")
@RequestMapping("/admin")
public class AdminActionController {

    private UserService userService;
    private AlertService alertService;

    @Autowired
    public AdminActionController(UserService userService, AlertService alertService) {
        this.userService = userService;
        this.alertService = alertService;
    }

    @GetMapping("/deleteuser")
    @ResponseBody
    public String deleteUser(@RequestParam("username") String username){

        userService.deleteUserByUsername(username);

        return "done";
    }

    @GetMapping("/removerole")
    @ResponseBody
    public String removeRole(@RequestParam("username") String username, @RequestParam("role") String role){

        User user = userService.findByUsername(username);

        Set<Role> roles = user.getRoles();
        roles.removeIf(x -> x.getRole().equals(role));

        user.setRoles(roles);

        userService.saveOrUpdate(user);

        return "done";

    }

    @GetMapping("/addrole")
    @ResponseBody
    public String addRole(@RequestParam("username") String username, @RequestParam("role") String role){

        User user = userService.findByUsername(username);

        Set<Role> roles = user.getRoles();

        Role newRole = new Role();

        newRole.setRole(role);

        roles.add(newRole);

        user.setRoles(roles);

        userService.saveOrUpdate(user);

        return "done";
    }

    @GetMapping("/makealert")
    public String makeAlert(Model model){

        model.addAttribute("alert", new Alert());

        return "admin/makealert";
    }

    @PostMapping("/makealert")
    public RedirectView makeAlertPost(@Valid Alert alert, BindingResult bindingResult, Principal principal){

        if (bindingResult.hasErrors()){
            System.out.println(bindingResult.getAllErrors());
        }

        alert.setAuthor(principal.getName());
        alertService.saveOrUpdate(alert);

        return new RedirectView("alert");
    }

    @GetMapping("/activatealert")
    public RedirectView activateAlert(@RequestParam("id") int id){

        Iterable<Alert> allAlerts = alertService.findAll();

        for (Alert alert : allAlerts){
            if (alert.isActive()){
                alert.setActive(false);
            }
        }

        alertService.saveOrUpdateAll(allAlerts);

        Alert alert = alertService.findById(id);

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();

        alert.setTimeActivated(dateFormat.format(date));
        alert.setActive(true);

        alertService.saveOrUpdate(alert);

        return new RedirectView("alert");
    }

    @GetMapping("/deletealert")
    public RedirectView deleteAlert(@RequestParam("id") int id){

        alertService.deleteAlert(alertService.findById(id));

        return new RedirectView("alert");
    }

    @GetMapping("/disablealert")
    public RedirectView disableAlert(){

        Iterable<Alert> allAlerts = alertService.findAll();

        for (Alert alert : allAlerts){
            if (alert.isActive()){
                alert.setActive(false);
            }
        }

        alertService.saveOrUpdateAll(allAlerts);

        return new RedirectView("alert");
    }

}
