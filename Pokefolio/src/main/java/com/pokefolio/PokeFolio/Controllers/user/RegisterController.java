package com.pokefolio.PokeFolio.Controllers.user;

import com.pokefolio.PokeFolio.Models.user.User;
import com.pokefolio.PokeFolio.Services.PersonalCardService;
import com.pokefolio.PokeFolio.Services.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.security.Principal;

@Controller
public class RegisterController {

    private UserService userService;
    private PersonalCardService personalCardService;

    public RegisterController(UserService userService, PersonalCardService personalCardService) {
        this.userService = userService;
        this.personalCardService = personalCardService;
    }

    @GetMapping("/register")
    public String showRegister(Model model){

        model.addAttribute("user", new User());

        return "user/register";
    }

    @PostMapping("/register")
    public String registerUser(@Valid User user, BindingResult bindingResult, Model model){
        if (bindingResult.hasErrors()){
            System.out.println(bindingResult.getAllErrors());
            return "user/register";
        }

        if (userService.isUserPresent(user.getEmail())){
           model.addAttribute("exists", true);

            return "user/register";
        }

        userService.createUser(user);

        return "user/login";
    }

    @PostMapping("/user/setupcollection")
    @ResponseBody
    public ResponseEntity<?> setupCollection(@Valid @RequestBody String message, Errors errors, Principal principal){
        User user = userService.findByUsername(principal.getName());
        user.setCollectionSetupStatus(1);
        userService.saveOrUpdate(user);
        personalCardService.setupCollection(user);
        user.setCollectionSetupStatus(2);
        userService.saveOrUpdate(user);
        if (errors.hasErrors()){
            return ResponseEntity.badRequest().body(errors.getAllErrors());
        }
        return ResponseEntity.ok("Complete");
    }

}
