package com.pokefolio.PokeFolio.Controllers.user;

import com.pokefolio.PokeFolio.Models.Notification;
import com.pokefolio.PokeFolio.Models.user.FriendRequest;
import com.pokefolio.PokeFolio.Models.user.notification.NotificationMaker;
import com.pokefolio.PokeFolio.Models.user.User;
import com.pokefolio.PokeFolio.Models.user.notification.notificationrecievers.AcceptFriendRequestReciever;
import com.pokefolio.PokeFolio.Models.user.notification.notificationrecievers.DeclineFriendRequestReciever;
import com.pokefolio.PokeFolio.Models.user.notification.notificationrecievers.FriendRequestReciever;
import com.pokefolio.PokeFolio.Models.user.notification.notificationrecievers.RemoveFriendReciever;
import com.pokefolio.PokeFolio.Services.FriendRequestService;
import com.pokefolio.PokeFolio.Services.UserService;
import net.bytebuddy.implementation.bind.annotation.BindingPriority;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.security.Principal;

@Controller
public class NotificationController {

    private UserService userService;
    private FriendRequestService friendRequestService;

    public NotificationController(UserService userService, FriendRequestService friendRequestService) {
        this.userService = userService;
        this.friendRequestService = friendRequestService;
    }

    @PostMapping("/userNotifications/makeFriendRequest")
    @ResponseBody
    public String makeFriendRequestPOST(@Valid FriendRequestReciever friendRequestReciever, Principal principal, BindingResult bindingResult){

        FriendRequest friendRequest = new FriendRequest();
        friendRequest.setReciever(friendRequestReciever.getReciever());
        friendRequest.setSender(principal.getName());

        friendRequestService.saveOrUpdate(friendRequest);

        User reciever = userService.findByUsername(friendRequestReciever.getReciever());
        reciever.addFriendRequest(friendRequest);
        userService.saveOrUpdate(reciever);

        makeFriendRequest(userService.findByUsername(principal.getName()), userService.findByUsername(friendRequestReciever.getReciever()));

        return "done";
    }

    @PostMapping("/userNotifications/acceptFriendRequest")
    @ResponseBody
    public String acceptFriendRequestPOST(@Valid AcceptFriendRequestReciever acceptFriendRequestReciever, Principal principal, BindingResult bindingResult){

        FriendRequest friendRequest = friendRequestService.findById(Integer.parseInt(acceptFriendRequestReciever.getId()));

        acceptFriendRequest(userService.findByUsername(friendRequest.getSender()), userService.findByUsername(principal.getName()), friendRequest);

        return "done";
    }

    @PostMapping("/userNotifications/declineFriendRequest")
    @ResponseBody
    public String declineFriendRequestPOST(@Valid DeclineFriendRequestReciever declineFriendRequestReciever, Principal principal, BindingResult bindingResult){

        FriendRequest friendRequest = friendRequestService.findById(Integer.parseInt(declineFriendRequestReciever.getId()));

        declineFriendRequest(userService.findByUsername(friendRequest.getSender()), userService.findByUsername(principal.getName()), friendRequest);

        return "done";
    }

    @PostMapping("/userNotifications/removeFriend")
    @ResponseBody
    public String RemoveFriend(@Valid RemoveFriendReciever removeFriendReciever, Principal principal, BindingResult bindingResult){

        User me = userService.findByUsername(principal.getName());
        User them = userService.findByUsername(removeFriendReciever.getUsername());

        me.removeFriend(them);
        them.removeFriend(me);

        userService.saveOrUpdate(me);
        userService.saveOrUpdate(them);

        return "done";
    }

    private void makeFriendRequest(User sender, User reciever){
        Notification request = NotificationMaker.makeFriendRequest(sender, reciever);
        reciever.addNotification(request);
        userService.saveOrUpdate(reciever);
    }

    private void acceptFriendRequest(User sender, User reciever, FriendRequest friendRequest){
        Notification request = NotificationMaker.makeFriendRequestAccepted(sender, reciever);

        sender.addFriend(reciever);
        reciever.addFriend(sender);

        reciever.removeFriendRequest(friendRequest);
        sender.removeFriendRequest(friendRequest);
        reciever.addNotification(request);
        userService.saveOrUpdate(reciever);
        userService.saveOrUpdate(sender);
    }

    private void declineFriendRequest(User sender, User reciever, FriendRequest friendRequest){
       sender.removeFriendRequest(friendRequest);
       reciever.removeFriendRequest(friendRequest);
       userService.saveOrUpdate(sender);
       userService.saveOrUpdate(reciever);
       friendRequestService.deleteFriendRequest(friendRequest);
    }
}
