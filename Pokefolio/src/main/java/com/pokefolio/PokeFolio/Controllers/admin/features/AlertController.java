package com.pokefolio.PokeFolio.Controllers.admin.features;

import com.pokefolio.PokeFolio.Services.AlertService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/admin")
@PreAuthorize("hasAnyRole('ADMIN')")
public class AlertController {

    private AlertService alertService;

    public AlertController(AlertService alertService) {
        this.alertService = alertService;
    }

    @GetMapping("/alert")
    public String alertFeature(Model model){

        model.addAttribute("alerts", alertService.findAll());

        return "admin/alert";
    }
}
