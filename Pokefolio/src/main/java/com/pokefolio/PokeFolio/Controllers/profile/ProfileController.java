package com.pokefolio.PokeFolio.Controllers.profile;

import com.pokefolio.PokeFolio.Controllers.profile.support.FileUpload.FileUploadService;
import com.pokefolio.PokeFolio.Controllers.profile.support.Statistics.StatisticGenerator;
import com.pokefolio.PokeFolio.Models.Notification;
import com.pokefolio.PokeFolio.Models.user.User;
import com.pokefolio.PokeFolio.Services.NotificationService;
import com.pokefolio.PokeFolio.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/myprofile")
public class ProfileController {

    private UserService userService;
    private StatisticGenerator statisticGenerator;
    private FileUploadService fileUploadService;

    @Autowired
    public ProfileController(UserService userService, StatisticGenerator statisticGenerator, FileUploadService fileUploadService) {
        this.userService = userService;
        this.statisticGenerator = statisticGenerator;
        this.fileUploadService = fileUploadService;
    }

    @GetMapping("/notificationCenter")
    public String myProfileNotificationCenter(Model model, Principal principal){

        User currentUser = userService.findByUsername(principal.getName());

        model.addAttribute("notifications", currentUser.getNotifications());

        for (Notification notification : currentUser.getNotifications()){
            notification.setReadStatus(1);
        }

        userService.saveOrUpdate(currentUser);

        return "profile/notificationcenter";
    }

    @GetMapping("/myFriends")
    public String myProfileFriends(Model model, Principal principal){

        User currentUser = userService.findByUsername(principal.getName());

        model.addAttribute("friends", currentUser.getFriends());

        return "profile/myfriends";
    }

    @GetMapping("/myFriendRequests")
    public String myProfileFriendRequests(Model model, Principal principal){

        User currentUser = userService.findByUsername(principal.getName());

        model.addAttribute("friendRequests", currentUser.getFriendRequests());

        return "profile/myfriendrequests";
    }

    @GetMapping("/setStatistics")
    public String myProfile(Model model, Principal principal){

        model.addAttribute("user", userService.findByUsername(principal.getName()));
        model.addAttribute("setStats", statisticGenerator.getSetStatistics(userService.findByUsername(principal.getName())));


        return "profile/setstatistics";
    }

    @PostMapping("/settings/bannerUpload")
    @ResponseBody
    public String updateBanner(@RequestParam(value = "file") MultipartFile file, Principal principal) throws IOException {

        fileUploadService.saveBanner(userService.findByUsername(principal.getName()), file);

        return "Done";
    }

    @PostMapping("/settings/avatarUpload")
    @ResponseBody
    public String updateAvatar(@RequestParam(value = "file") MultipartFile file, Principal principal) throws IOException {

        fileUploadService.saveAvatar(userService.findByUsername(principal.getName()), file);

        return "Done";
    }

    @GetMapping("/profileSettings")
    public String profileSettings(){
        return "profile/profilesettings";
    }


    @GetMapping("")
    public String profileHome(){
        return "profile/profilehome";
    }
}
