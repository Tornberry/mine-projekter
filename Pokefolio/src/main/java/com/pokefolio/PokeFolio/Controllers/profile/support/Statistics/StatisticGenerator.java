package com.pokefolio.PokeFolio.Controllers.profile.support.Statistics;

import com.pokefolio.PokeFolio.Models.CardSet;
import com.pokefolio.PokeFolio.Models.user.User;
import com.pokefolio.PokeFolio.Models.user.collection.PersonalCard;
import com.pokefolio.PokeFolio.Services.PersonalCardService;
import com.pokefolio.PokeFolio.Services.SetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StatisticGenerator {

    private PersonalCardService personalCardService;
    private SetService setService;

    @Autowired
    public StatisticGenerator(PersonalCardService personalCardService, SetService setService) {
        this.personalCardService = personalCardService;
        this.setService = setService;
    }

    public List<SetStatistic> getSetStatistics(User user){
            Iterable<CardSet> allSets = setService.findAll();
            return getAllSetsOwnPercent(allSets, user);
    }

    private List<SetStatistic> getAllSetsOwnPercent(Iterable<CardSet> sets, User user){

        List<SetStatistic> returnStats = new ArrayList<>();

        for (CardSet set : sets){
            SetStatistic setStat = new SetStatistic(set.getName());
            setStat.setTotalCards(set.getTotalCards());

            float iOwn = 0;
            for (PersonalCard personalCard : personalCardService.getAllPersonalCardsByUserAndFromSet(user, set.getName())){
                if (!(personalCard.getAmount() == 0)){
                    iOwn++;
                }
            }

            setStat.setOwned((int)iOwn);
            setStat.setOwnPercent(percentCalc(Float.parseFloat(set.getTotalCards()), iOwn));

            returnStats.add(setStat);
        }

        return returnStats;
    }

    private String percentCalc(float max, float variable){
        float percent = (float)(variable/max)*100;

        return String.valueOf(percent + "%");
    }

}
