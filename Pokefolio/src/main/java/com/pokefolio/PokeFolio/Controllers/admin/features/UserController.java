package com.pokefolio.PokeFolio.Controllers.admin.features;

import com.pokefolio.PokeFolio.Models.user.Role;
import com.pokefolio.PokeFolio.Services.UserService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Controller
@PreAuthorize("hasAnyRole('ADMIN')")
@RequestMapping("/admin")
public class UserController {

    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/userlist")
    public String getUserList(Model model){

        model.addAttribute("users", userService.findAll());

        return "admin/userlist";
    }

    @GetMapping("/changeroles")
    public String getChangeRolesPage(Model model, @RequestParam("username") String username){

        model.addAttribute("user", userService.findByUsername(username));

        List<String> roles = new ArrayList<>();

        for (Role role : userService.findByUsername(username).getRoles()){
            roles.add(role.getRole());
        }

        model.addAttribute("roles", roles);

        return "admin/changeroles";
    }



}
