package com.pokefolio.PokeFolio.Controllers.cards.search.postRecievers;

public class AmountReciever {
    public String PCID;
    public int amountType;

    public AmountReciever(String PCID, int amountType) {
        this.PCID = PCID;
        this.amountType = amountType;
    }

    public String getPCID() {
        return PCID;
    }

    public void setPCID(String PCID) {
        this.PCID = PCID;
    }

    public int getAmountType() {
        return amountType;
    }

    public void setAmountType(int amountType) {
        this.amountType = amountType;
    }
}
