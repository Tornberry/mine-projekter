package com.pokefolio.PokeFolio.Controllers.home;

import com.pokefolio.PokeFolio.Services.AlertService;
import com.pokefolio.PokeFolio.Services.CardService;
import com.pokefolio.PokeFolio.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Controller
@RequestMapping("/")
public class HomeController {

    private CardService cardService;
    private UserService userService;
    private AlertService alertService;

    @Autowired
    public HomeController(CardService cardService, UserService userService, AlertService alertService) {
        this.cardService = cardService;
        this.userService = userService;
        this.alertService = alertService;
    }

    @GetMapping("/")
    public String showHome(Principal principal, Model model) {

        model.addAttribute("currentUser", userService.findByUsername(principal.getName()));
        model.addAttribute("alerts", alertService.findAll());

        return "index";
    }

}
