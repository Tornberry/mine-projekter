package com.pokefolio.PokeFolio.Controllers.data.support;

import com.pokefolio.PokeFolio.Models.CardSet;
import com.pokefolio.PokeFolio.Services.CardService;
import org.springframework.stereotype.Service;

@Service
public class dataAdapter {

    private CardService cardService;


    public CardSet adaptSet(CardSet cardSet){

        CardSet newCardSet = new CardSet();
        newCardSet.setExpandedLegal(cardSet.getExpandedLegal());
        newCardSet.setSeries(cardSet.getSeries());
        newCardSet.setReleaseDate(cardSet.getReleaseDate());
        newCardSet.setPtcgocode(cardSet.getPtcgocode());
        newCardSet.setStandardLegal(cardSet.getStandardLegal());
        newCardSet.setName(cardSet.getName());
        newCardSet.setTotalCards(cardSet.getTotalCards());
        newCardSet.setLogoUrl(cardSet.getLogoUrl());
        newCardSet.setSymbolUrl(cardSet.getSymbolUrl());
        newCardSet.setCode(cardSet.getCode());

        return newCardSet;
    }
}
