package com.pokefolio.PokeFolio.Repositories;

import com.pokefolio.PokeFolio.Models.user.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface roleRepository extends JpaRepository<Role, String> {
}
