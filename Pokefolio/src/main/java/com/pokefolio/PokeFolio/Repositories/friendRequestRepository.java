package com.pokefolio.PokeFolio.Repositories;

import com.pokefolio.PokeFolio.Models.user.FriendRequest;
import org.springframework.data.jpa.repository.JpaRepository;

public interface friendRequestRepository extends JpaRepository<FriendRequest, Integer> {
    FriendRequest findBySenderAndReciever(String sender, String reciever);
    FriendRequest findById(int id);
}
