package com.pokefolio.PokeFolio.Repositories;

import com.pokefolio.PokeFolio.Models.CardSet;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface setRepository extends JpaRepository<CardSet, Integer> {

    List<CardSet> findBySeries(String series);
    CardSet findByName(String name);

}
