package com.pokefolio.PokeFolio.Repositories;

import com.pokefolio.PokeFolio.Models.Card;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface cardRepository extends JpaRepository<Card, Integer> {
    List<Card> findByCardSet(String set);
    List<Card> findByNameContaining(String name);
    Card findById(String id);
    Card findByCardId(String id);
}
