package com.pokefolio.PokeFolio.Repositories;

import com.pokefolio.PokeFolio.Models.user.User;
import com.pokefolio.PokeFolio.Models.user.collection.PersonalCard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface personalCardRepository extends CrudRepository<PersonalCard, Integer> {
    Iterable<PersonalCard> findByUserAndFromSet(User user, String fromSet);
    PersonalCard findById(int id);
}
