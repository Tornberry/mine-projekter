package com.pokefolio.PokeFolio.Repositories;

import com.pokefolio.PokeFolio.Models.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface userRepository extends JpaRepository<User, String> {

    User findByEmail(String email);
    User findByUsername(String username);
    void deleteByUsername(String username);
}
