package com.pokefolio.PokeFolio.Repositories;

import com.pokefolio.PokeFolio.Models.MainSet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface mainSetRepository extends JpaRepository<MainSet, Integer> {
}
