package com.pokefolio.PokeFolio.Repositories;

import com.pokefolio.PokeFolio.Models.Notification;
import org.springframework.data.jpa.repository.JpaRepository;

public interface notificationRepository extends JpaRepository<Notification, Integer> {

}
