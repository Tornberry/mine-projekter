package com.pokefolio.PokeFolio.Repositories;

import com.pokefolio.PokeFolio.Models.website.Alert;
import org.springframework.data.repository.CrudRepository;

public interface alertRepository extends CrudRepository<Alert, Integer>{
    Alert findById(int id);
}
