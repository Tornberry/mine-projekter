package com.pokefolio.PokeFolio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PokeFolioApplication {

	public static void main(String[] args) {
		SpringApplication.run(PokeFolioApplication.class, args);
	}
}
